package services.helpers;

public class Constants {
	/** Pagination */
	public static final int PageSize = 10;
	
	/*Path for upload image member*/
	public static final String PATH = System.getProperty("user.dir")+"/src/main/java/assets/";
}
