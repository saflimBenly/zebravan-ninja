package services.helpers;

public class Pagination {
	static int maxView;
	static int j;
	
	public static String generatePage(int total, int page, int pageSize) {
		String result = "";

		int all = 0;
		if (total % pageSize == 0) {// jika habis dibagi pageSize a.k.a tidak
									// terdapat modulo
			all = total / pageSize;
			result += generateLooping(all, page, pageSize);

		} else {// jika masih sisa a.k.a terdapat modulo
			all = (total / pageSize) + 1;
			result += generateLooping(all, page, pageSize);
		}

		return result;
	}
	
	private static String generateLooping(int all, int page, int pageSize) {
		String result = "";
		
		result += "<div class=\"clearfix\"></div>";
		result += "<ul class=\"pagination pull-right\">";
		
		if (page == 1) {
			result += "<li class=\"disabled\"><a href=\"#\"><span class=\"fa fa-chevron-left\"></span></a></li>";
		}
		if (page > 1) {
			int prev = page - 1;
			result += "<li><a href=\"?page=" + prev + "\"><span class=\"fa fa-chevron-left\"></span></a></li>";
		}
		
		if (page == 1) {
			if (all >= 5) {
				maxView = 5;
				j = 1;
			}
			if (all < 5) {
				j = 1;
				maxView = all;
			}

		}

		if (maxView >= 5) {
			if (page == maxView) {
				maxView += 1;
				j += 1;

				if (maxView > all) {
					maxView -= 1;
					j -= 1;
				}

			}

			if (maxView > 5) {
				if (page == j) {
					maxView -= 1;
					j -= 1;
				}

				if (j < 1) {
					j = 1;
					maxView = 5;
				}

			}

		}
		if (page == all) {
			maxView = all;
			if (page > 5) {
				j = maxView - 4;
			}

		}
		
		for (int i = j; i <= maxView; i++) {
			if(page==i){
				result += "<li class=\"active\"><a href=\"?page=" + i + "\">"+ i +"</a></li>";
			}else{
				result += "<li><a href=\"?page=" + i + "\">"+ i +"</a></li>";
			}
			
		}
		
		if (page == all) {
			result += "<li class=\"disabled\"><a href=\"#\"><span class=\"fa fa-chevron-right\"></span></a></li>";
		}

		if (page < all) {
			int next = page + 1;
			result += "<li><a href=\"?page=" + next + "\"><span class=\"fa fa-chevron-right\"></span></a></li>";
		}
		if (all == 0) {
			result = "";
		}
		
		result += "</ul>";
		return result;
	}
}
;