package services.filters;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import controllers.AdminController;
import ninja.Context;
import ninja.FilterChain;
import ninja.Filter;
import ninja.Result;
import ninja.Results;

public class AdminFilter implements Filter {
    public final String ISADMIN = "isAdmin";
	private Log log = LogFactory.getLog(AdminFilter.class);    
    
	@Override
	public Result filter(FilterChain chain, Context context) {
		// if we got no cookies we break:
		if (context.getSession() == null
				|| context.getSession().get(ISADMIN) == null 
				|| !context.getSession().get(ISADMIN).equals("true")) {

			return Results.forbidden().html().template("/views/system/403forbidden.ftl.html");

		} else {
			return chain.next(context);
		}
	}

}
