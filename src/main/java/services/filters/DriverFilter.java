package services.filters;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import controllers.AdminController;
import ninja.Context;
import ninja.FilterChain;
import ninja.Filter;
import ninja.Result;
import ninja.Results;

public class DriverFilter implements Filter {
    public final String ISDRIVER = "isDriver";
	private Log log = LogFactory.getLog(DriverFilter.class);    
    
	@Override
	public Result filter(FilterChain chain, Context context) {
		// if we got no cookies we break:
		if (context.getSession() == null
				|| context.getSession().get(ISDRIVER) == null 
				|| !context.getSession().get(ISDRIVER).equals("true")) {

			return Results.forbidden().html().template("/views/system/403forbidden.ftl.html");

		} else {
			return chain.next(context);
		}
	}

}
