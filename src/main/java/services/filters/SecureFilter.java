package services.filters;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import controllers.AdminController;
import ninja.Context;
import ninja.FilterChain;
import ninja.Filter;
import ninja.Result;
import ninja.Results;

public class SecureFilter implements Filter {
    /** If a username is saved we assume the session is valid */
    public final String USERNAME = "username";
	private Log log = LogFactory.getLog(SecureFilter.class);    
    
	@Override
	public Result filter(FilterChain chain, Context context) {
		// if we got no cookies we break:
		if (context.getSession() == null
				|| context.getSession().get(USERNAME) == null) {

			return Results.forbidden().html().template("/views/system/403forbidden.ftl.html");

		} else {
			return chain.next(context);
		}
	}

}
