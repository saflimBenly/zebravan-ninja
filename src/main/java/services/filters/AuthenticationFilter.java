
package services.filters;

import com.google.inject.Inject;
import io.jsonwebtoken.Claims;
import ninja.Context;
import ninja.Filter;
import ninja.FilterChain;
import ninja.Result;
import ninja.Results;
import ninja.utils.NinjaProperties;
import services.utils.AuthenticationUtils;

/**
 *
 * @author john
 */
public class AuthenticationFilter implements Filter {

    @Inject
    private NinjaProperties ninjaProperties;
    
    @Override
    public Result filter(FilterChain filterChain, Context context) {
        if( !AuthenticationUtils.hasAuthenticationToken(context)) 
            return Results.unauthorized();
         
        String applicationSecret = ninjaProperties.get("application.secret");
        Claims claims = AuthenticationUtils.parseToken(context, applicationSecret);

        //check the timestamp
        if( AuthenticationUtils.isExpired(claims)) {
            return Results.unauthorized();
        }

        String email = claims.getSubject();
        //grab the user from the DB using the email

        return filterChain.next(context);
    }
 
}
