/**
 * Copyright (C) 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package conf;

import com.google.inject.Inject;

import controllers.AdminController;
import controllers.ApiController;
import controllers.ApplicationController;
import controllers.BookRideController;
import controllers.DriverController;
import controllers.LoginLogoutController;
import controllers.LookupController;
import controllers.MemberController;
import ninja.AssetsController;
import ninja.Results;
import ninja.Router;
import ninja.application.ApplicationRoutes;
import ninja.utils.NinjaProperties;

public class Routes implements ApplicationRoutes {

	@Inject
	NinjaProperties ninjaProperties;

	/**
	 * Using a (almost) nice DSL we can configure the router.
	 * 
	 * The second argument NinjaModuleDemoRouter contains all routes of a
	 * submodule. By simply injecting it we activate the routes.
	 * 
	 * @param router
	 *            The default router of this application
	 */
	@Override
	public void init(Router router) {
//		For CORS
		router.OPTIONS().route("/.*").with(ApplicationController.class, "cors"); // Pre-flight end point
		router.POST().route("/authenticate").with(ApplicationController.class, "authenticate");

//		From Example
		router.POST().route("/person").with(ApplicationController.class, "person");
		router.GET().route("/getblog/{id}").with(ApplicationController.class, "getBlog");
		router.POST().route("/saveblog").with(ApplicationController.class, "saveBlog");

		// puts test data into db:
		// if (!ninjaProperties.isProd()) {
		// router.GET().route("/setup").with(ApplicationController.class,
		// "setup");
		// }
		router.GET().route("/index").with(ApplicationController.class, "index");
		router.POST().route("/contact").with(ApplicationController.class, "contact");

		router.GET().route("/booked/{id}").with(ApplicationController.class, "bookedRide");
//		router.POST().route("/booked").with(ApplicationController.class, "bookedRegister");
		
		//NEW ROUTE
		router.GET().route("/routes").with(BookRideController.class, "searchRoute");
		router.POST().route("/search/routes").with(BookRideController.class, "searchRouteSubmit");
		router.POST().route("/booked").with(BookRideController.class, "bookedSubmit");
		
		//OLD ROUTE
		router.GET().route("/booking").with(BookRideController.class, "bookingAvailableList");
		router.GET().route("/ride").with(BookRideController.class, "rideAvailableList");
		router.GET().route("/ride/{id}").with(BookRideController.class, "rideView");
		router.POST().route("/ride/sendloc").with(BookRideController.class, "rideSendLocation");
		router.GET().route("/ride/getloc/{id}").with(BookRideController.class, "rideGetLocation");
		router.GET().route("/ride/chat/{id}").with(BookRideController.class, "rideChat");
		router.POST().route("/ride/delete").with(BookRideController.class, "rideDelete");
		router.GET().route("/ride/edit/{id}").with(BookRideController.class, "rideEditView");
		router.POST().route("/ride/edit").with(BookRideController.class, "rideEdit");
		
		///////////////////////////////////////////////////////////////////////
		// Landing Page
		///////////////////////////////////////////////////////////////////////
		//router.POST().route("/landing/save").with(ApplicationController.class, "landingSave");
//		router.GET().route("/landing").with(Results.html().template("/assets/landing/index.html"));
//		router.GET().route("/terms").with(Results.html().template("/assets/landing/terms.html"));
//		router.GET().route("/policy").with(ApplicationController.class, "policy");

		router.POST().route("/joinNewsLetter").with(LoginLogoutController.class, "joinNewsLetter");
		///////////////////////////////////////////////////////////////////////
		// Login / Logout
		///////////////////////////////////////////////////////////////////////
		router.GET().route("/login").with(LoginLogoutController.class, "login");
		router.POST().route("/login").with(LoginLogoutController.class, "loginPost");
		router.POST().route("/loginSNS").with(LoginLogoutController.class, "loginSNS");
		router.POST().route("/signupSNS").with(LoginLogoutController.class, "signupSNS");
		router.GET().route("/logout").with(LoginLogoutController.class, "logout");

		router.GET().route("/sign-up").with(LoginLogoutController.class, "signUp");
		router.POST().route("/sign-up").with(LoginLogoutController.class, "signUpPost");
		router.POST().route("/sign-up-fb").with(LoginLogoutController.class, "signUpFB");

		router.GET().route("/verification/{key}").with(LoginLogoutController.class, "verificationAccount");

		router.GET().route("/forget").with(LoginLogoutController.class, "forgetPassword");
		router.POST().route("/forget").with(LoginLogoutController.class, "sendForgetLink");

		router.GET().route("/reset-password").with(LoginLogoutController.class, "resetForm");
		router.POST().route("/reset-password").with(LoginLogoutController.class, "resetPassword");
		
		router.GET().route("/callback").with(LoginLogoutController.class, "callback");
		
		//member View
		router.GET().route("/member").with(MemberController.class, "memberView");
		router.GET().route("/callback").with(LoginLogoutController.class, "callback");
		
		///////////////////////////////////////////////////////////////////////
		// Admin
		///////////////////////////////////////////////////////////////////////
		router.GET().route("/admin").with(AdminController.class, "adminFront");
		// Member
		router.GET().route("/admin/member").with(AdminController.class, "memberList");
		router.GET().route("/admin/member/{id}").with(AdminController.class, "memberView");
		router.POST().route("/admin/member/{id}").with(AdminController.class, "updateMember");
		router.POST().route("/uploadImage").with(AdminController.class, "uploadImage");
		router.GET().route("/retriveImage/{id}").with(AdminController.class, "retriveImage");
		// Partner
		router.GET().route("/admin/partner").with(AdminController.class, "partnerList");
		// router.GET().route("/admin/partner/{id}").with(AdminController.class,
		// "partnerDetail");
		// router.POST().route("/admin/partner/{id}").with(AdminController.class,
		// "partnerUpdate");

		// ZebRoute
		router.GET().route("/admin/route").with(AdminController.class, "routeList");
		router.GET().route("/admin/route/{id}").with(AdminController.class, "routeView");
		router.POST().route("/admin/route/upload").with(AdminController.class, "routeUploadImg");
		router.POST().route("/admin/route").with(AdminController.class, "routeSave");

		// Route - Daily Schedule
		router.GET().route("/admin/schedule/{id}").with(AdminController.class, "scheduleView");
		router.POST().route("/admin/schedule").with(AdminController.class, "scheduleSave");

		router.GET().route("/admin/schedule").with(AdminController.class, "dailyScheduleView");
		router.GET().route("/admin/schedule/{id}").with(AdminController.class, "dailyScheduleDetail");

		//Driver
		router.GET().route("/admin/driver").with(DriverController.class, "driverList");
		router.POST().route("/admin/driver").with(DriverController.class, "driverSave");
		router.GET().route("/admin/driver/{id}").with(DriverController.class, "driverView");
		router.GET().route("/ride-list").with(DriverController.class, "driverRideList");
		
		router.POST().route("/admin/vehicle").with(DriverController.class, "vehicleSave");
		router.GET().route("/getvehicle/{id}").with(DriverController.class, "driverVans");
		
		router.GET().route("/admin/zebvan").with(DriverController.class, "zebvanList");

		router.GET().route("/admin/allocate").with(DriverController.class, "allocateList");
		router.GET().route("/admin/allocate/{id}").with(DriverController.class, "allocateView");
		router.POST().route("/admin/allocate").with(DriverController.class, "allocateSave");
		
		router.GET().route("/getlookup").with(LookupController.class, "getLookup");

		router.GET().route("/admin/lookupGroupList").with(LookupController.class, "lookupGroupList");
		router.POST().route("/admin/addLookupGroup").with(LookupController.class, "addLookupGroup");
		router.POST().route("/admin/deleteLookupGroup").with(LookupController.class, "deleteLookupGroup");
		router.POST().route("/admin/updateLookupGroup").with(LookupController.class, "updateLookupGroup");

		router.GET().route("/admin/lookupList").with(LookupController.class, "lookupList");
		router.POST().route("/admin/addLookup").with(LookupController.class, "addLookup");
		router.POST().route("/admin/deleteLookup").with(LookupController.class, "deleteLookup");
		router.POST().route("/admin/updateLookup").with(LookupController.class, "updateLookup");

		router.GET().route("/admin/categoryList").with(AdminController.class, "categoryList");

		// Search
		router.GET().route("/search").with(ApplicationController.class, "search");

		///////////////////////////////////////////////////////////////////////
		// From Example  : Api for management of software
		///////////////////////////////////////////////////////////////////////
		router.GET().route("/api/admin/member/{id}").with(ApiController.class, "getMemberViewJson");
		router.GET().route("/api/{username}/articles.json").with(ApiController.class, "getArticlesJson");
		// router.GET().route("/api/{username}/article/{id}.json").with(ApiController.class,
		// "getArticleJson");
		// router.GET().route("/api/{username}/articles.xml").with(ApiController.class,
		// "getArticlesXml");
		// router.POST().route("/api/{username}/article.json").with(ApiController.class,
		// "postArticleJson");
		// router.POST().route("/api/{username}/article.xml").with(ApiController.class,
		// "postArticleXml");

		///////////////////////////////////////////////////////////////////////
		// Assets (pictures / javascript)
		///////////////////////////////////////////////////////////////////////
		router.GET().route("/assets/webjars/{fileName: .*}").with(AssetsController.class, "serveWebJars");
		router.GET().route("/assets/{fileName: .*}").with(AssetsController.class, "serveStatic");

		///////////////////////////////////////////////////////////////////////
		// Index / Catchall shows index page
		///////////////////////////////////////////////////////////////////////
		router.GET().route("/.*").with(ApplicationController.class, "index");
	}

}
