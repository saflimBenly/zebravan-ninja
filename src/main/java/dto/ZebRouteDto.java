package dto;

public class ZebRouteDto {
	
	public Long id;
	public String code;
	public String name;
	public String fromname;
	public String frompoint;
	public String fromdisplay;
	public String toname;
	public String topoint;
	public String todisplay;
	public int price;
	public String description;
	public String[] locName;
	public String[] locPoint;
	public String[] locDisplay;

	public ZebRouteDto(){}

}
