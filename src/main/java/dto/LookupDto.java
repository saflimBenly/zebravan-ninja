package dto;

/**
 * Created by safaridinsal on 30/07/2015.
 */
public class LookupDto {
    public String code;
    public String name;
    public String description;
    public String lookupGroupCode;
}
