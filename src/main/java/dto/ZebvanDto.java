package dto;

import java.sql.Date;
import java.util.ArrayList;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import models.BookTrip;
import antlr.collections.List;

public class ZebvanDto {
	
	//--Save
	private Long id;
	private Long driverId;
	private String driverName; 
	
	private String code;
	private String name;

	@NotNull
	@Size(min=5)
	private String plateNo;
	private String make;
	private String model;
	private String color;
	private int year;
	private int seatAvailable;
	private String stnkNo;
	private Date stnkExp;
	
	private String kirNo;
	private Date kirExp;
	@Size(min=5)
	private String description;
	private String imagePath;
	
	private ArrayList<BookTrip> booktrips;
	
	public ZebvanDto(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getDriverId() {
		return driverId;
	}

	public void setDriverId(Long driverId) {
		this.driverId = driverId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPlateNo() {
		return plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getSeatAvailable() {
		return seatAvailable;
	}

	public void setSeatAvailable(int seatAvailable) {
		this.seatAvailable = seatAvailable;
	}

	public String getStnkNo() {
		return stnkNo;
	}

	public void setStnkNo(String stnkNo) {
		this.stnkNo = stnkNo;
	}

	public Date getStnkExp() {
		return stnkExp;
	}

	public void setStnkExp(Date stnkExp) {
		this.stnkExp = stnkExp;
	}

	public String getKirNo() {
		return kirNo;
	}

	public void setKirNo(String kirNo) {
		this.kirNo = kirNo;
	}

	public Date getKirExp() {
		return kirExp;
	}

	public void setKirExp(Date kirExp) {
		this.kirExp = kirExp;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public ArrayList<BookTrip> getBooktrips() {
		return booktrips;
	}

	public void setBooktrips(ArrayList<BookTrip> booktrips) {
		this.booktrips = booktrips;
	}

}
