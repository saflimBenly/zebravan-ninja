package dto;

import java.util.Date;

public class SearchRouteDto {
	
	public Long id;
	
	public Long routeList;
	public Date routeDt;
	public String trip;
	public Long passenger;
	
	public SearchRouteDto(){}

}
