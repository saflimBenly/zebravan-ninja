package dto;

import java.util.Date;
import java.util.Map;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class DriverScheduleDto {
	
	//--View
	public Date	scheduledate;
	public Map<String, Map>	routeType;
	
	//--Save
	public Long driverid;
	
	@NotNull
	@Size(min=7)
	public String drivername;
	
	@NotNull
	@Pattern(regexp="[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,10}$")
	@Size(min=7)
	public String driveremail;
	
	@Size(min=7)
	public String driverphone;
	
	@Size(min=7)
	public String devicekey;
	
	@Size(min=7)
	public String ktpno;
	
	@Size(min=7)
	public String simno;
	
	public Long defaultvan;
	public Long[] tripid;

	public DriverScheduleDto(){}

}
