package dto;


public class DailyScheduleDto {
	
	public Long		id;
	public Long		routeid;
	public Long[]	scheduleid;
	public String[]	tripdate;
	public String[]	triptime;
	public String[]	status;
	public Integer[] allocation;

	public DailyScheduleDto(){}

}
