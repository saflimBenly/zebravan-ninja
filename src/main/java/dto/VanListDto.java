package dto;

public class VanListDto {
	
	private Long driver_id;
	
	public VanListDto(){}

	public Long getDefaultVanId() {
		return driver_id;
	}

	public void setDefaultVanId(Long driver_id) {
		this.driver_id = driver_id;
	}
		
}
