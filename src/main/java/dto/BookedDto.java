package dto;

import java.sql.Date;
import java.sql.Time;


public class BookedDto {
	
	public Long	routeId;
	public Integer routePrice;
	public String[] scheduleId;
	public String[] scheduleTime;
	public String[] scheduleDate;
	
	public String[] pickup;

	public Long	tripId;
	public Long[] bookedId;
	public String[] booked;
	public String[] allocated;
	
	public Long idBooked;
	public String routeNameBooked;
	public String routeDescBooked;
	public String dateBooked;
	public String timeBooked;
	public String pickupBooked;

	public BookedDto(){}

}
