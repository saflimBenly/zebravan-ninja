package dto;

/**
 * Created by safaridinsal on 9/09/2016
 */
public class LatLngDto {
	public Long driverId;
    public double lat;
    public double lng;

    public LatLngDto(){}

    public LatLngDto(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }
}
