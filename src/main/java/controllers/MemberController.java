/**
 * Copyright (C) 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers;

import java.util.HashMap;
import java.util.Map;

import models.Member;
import ninja.Context;
import ninja.FilterWith;
import ninja.Result;
import ninja.Results;
import ninja.utils.NinjaProperties;
import services.filters.CrossOriginAccessControlFilter;
import services.filters.SecureFilter;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import dao.GenericDao;

//@FilterWith(SecureFilter.class)

@Singleton
@FilterWith(CrossOriginAccessControlFilter.class)
public class MemberController {

    @Inject 
    private NinjaProperties ninjaProperties;
    
    @Inject
    GenericDao dao;

    @Inject
    MailController mailController;

    public MemberController() {

    }

	@FilterWith(SecureFilter.class)
	public Result memberView(Context context){
		
	    final String MEMNO = "memNo";
	    Member member = new Member();
		Map<String, Map> dropdown = new HashMap<String, Map>();
	    
	    try {
			String memId = context.getSession().get(MEMNO);
			if (null == memId || "".equals(memId)){
				throw new Exception();
			}
			member = dao.find(Member.class, Long.parseLong(memId));

			HashMap<String, String> genderLookup = new HashMap<String, String>();
			genderLookup.put("L", "Laki-laki");
			genderLookup.put("P", "Perempuan");
			
			dropdown.put("gender", genderLookup);
			
		} catch (Exception e) {
			e.printStackTrace();
			context.getFlashScope().error("error");
			return Results.redirect("/login");
		}
		Result result = Results.html()
				.render("member", member)
				.render("dropdown", dropdown);
	    return result;
    }
}
