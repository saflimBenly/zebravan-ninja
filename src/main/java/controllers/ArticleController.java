/**
 * Copyright (C) 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers;

import dao.GenericDao;
import models.Article;
import dto.ArticleDto;
import models.Member;
import ninja.Context;
import ninja.FilterWith;
import ninja.Result;
import ninja.Results;
import ninja.SecureFilter;
import ninja.params.PathParam;
import ninja.validation.JSR303Validation;
import ninja.validation.Validation;
import services.utils.LoggedInUser;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class ArticleController {
    
    @Inject
    GenericDao dao;

    ///////////////////////////////////////////////////////////////////////////
    // Show article
    ///////////////////////////////////////////////////////////////////////////
    public Result articleShow(@PathParam("id") Long id) {

        Article article = null;

        if (id != null) {

            article = dao.find(Article.class, id);

        }

        return Results.html().render("article", article);

    }
    
    ///////////////////////////////////////////////////////////////////////////
    // Create new article
    ///////////////////////////////////////////////////////////////////////////
    @FilterWith(SecureFilter.class)
    public Result articleNew() {

        return Results.html();

    }

    @FilterWith(SecureFilter.class)
    public Result articleNewPost(@LoggedInUser String username,
                                 Context context,
                                 @JSR303Validation ArticleDto articleDto,
                                 Validation validation) {

        if (validation.hasViolations()) {
            context.getFlashScope().error("error.correct.field");
            return Results.redirect("/article/new");

        } else {
            Member member = dao.findOneByProperty(Member.class, "username", username);
            Article article = new Article(member, articleDto.title, articleDto.content);

            dao.save(article);
            context.getFlashScope().success("success.article.created");
            return Results.redirect("/");

        }

    }

}
