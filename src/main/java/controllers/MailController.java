package controllers;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.internal.ErrorsException;
import models.MailObj;
import ninja.postoffice.Mail;
import ninja.postoffice.Postoffice;

import javax.mail.internet.AddressException;

public class MailController {

    @Inject
    Provider<Mail> mailProvider;

    @Inject
    Postoffice postoffice;

    public void sendMail() {

        Mail mail = mailProvider.get();

        // fill the mail with content:
        mail.setSubject("subject");

        mail.setFrom("mamansuraman@maman.com");

//        mail.addReplyTo("replyTo1@domain");
//        mail.addReplyTo("replyTo2@domain");

        mail.setCharset("utf-8");
//        mail.addHeader("header1", "value1");
//        mail.addHeader("header2", "value2");

        mail.addTo("javlinbukanjavelin@gmail.com");
        mail.addTo("to2@domain");

//        mail.addCc("cc1@domain");
//        mail.addCc("cc2@domain");
//
//        mail.addBcc("bcc1@domain");
//        mail.addBcc("bcc2@domain");

        mail.setBodyHtml("bodyHtml");

        mail.setBodyText("bodyText");

        // finally send the mail
        try {
            postoffice.send(mail);
        } catch (ErrorsException | AddressException e) {
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void sendMail(MailObj mailObj) {
        Mail mail = mailProvider.get();

        // fill the mail with content:
        mail.setSubject(mailObj.getSubject());

        mail.setFrom(mailObj.getFrom());

        mail.setCharset("utf-8");

        for (int i = 0; i < mailObj.getAddTo().length; i++) {
            mail.addTo(mailObj.getAddTo()[i]);
        }

        mail.setBodyHtml(mailObj.getBodyHtml());
        
        // finally send the mail
        try {
            postoffice.send(mail);
        } catch (ErrorsException | AddressException e) {
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}