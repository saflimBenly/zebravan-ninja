/**
 * Copyright (C) 2012 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers;

import java.util.HashMap;
import java.util.Map;

import models.Address;
import models.Article;
import models.Member;
import ninja.FilterWith;
import ninja.Result;
import ninja.Results;
import services.filters.AdminFilter;
import ninja.params.PathParam;
import services.utils.LoggedInUser;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import dao.GenericDao;
import dto.ArticleDto;
import dto.ArticlesDto;

@Singleton
public class ApiController {

    @Inject
    GenericDao dao;

    public Result getArticlesJson() {

        ArticlesDto articlesDto = new ArticlesDto();
        articlesDto.articles = dao.findAll(Article.class);

        return Results.json().render(articlesDto);

    }

    public Result getMemberViewJson(@PathParam("id") Long id) {

		Member member = dao.find(Member.class, id);
		
		Map<String, Map> dropdown = new HashMap<String, Map>();
		HashMap<String, String> genderLookup = new HashMap<String, String>();
		genderLookup.put("L", "Laki-laki");
		genderLookup.put("P", "Perempuan");
		genderLookup.put("T", "Tidak Tahu");
		
		dropdown.put("gender", genderLookup);
		
		Result result = Results.json().render("member",member)
				.render("dropdown", dropdown);
	    return result;

    }

    public Result getArticlesXml() {

        ArticlesDto articlesDto = new ArticlesDto();
        articlesDto.articles = dao.findAll(Article.class);

        return Results.xml().render(articlesDto);

    }
    
    public Result getArticleJson(@PathParam("id") Long id) {
    
        Article article = dao.find(Article.class, id);
        
        return Results.json().render(article);
    
    }

    @FilterWith(AdminFilter.class)
    public Result postArticleJson(@LoggedInUser String username,
                                  ArticleDto articleDto) {

        Member member = dao.findOneByProperty(Member.class, "username", username);
        Article article = new Article(member, articleDto.title, articleDto.content);

        Long saveId = dao.save(article);

        boolean succeeded = (saveId > 0)? true:false;

        if (!succeeded) {
            return Results.notFound();
        } else {
            return Results.json();
        }

    }

    @FilterWith(AdminFilter.class)
    public Result postArticleXml(@LoggedInUser String username,
                                 ArticleDto articleDto) {

        Member member = dao.findOneByProperty(Member.class, "username", username);
        Article article = new Article(member, articleDto.title, articleDto.content);

        Long saveId = dao.save(article);

        boolean succeeded = (saveId > 0)? true:false;


        if (!succeeded) {
            return Results.notFound();
        } else {
            return Results.xml();
        }

    }
}
