/**
 * Copyright (C) 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.RollbackException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import models.Address;
import models.Contact;
import models.DailySchedule;
import models.DailyScheduleView;
import models.Member;
import models.MemberSocmed;
import models.Partner;
import models.RoutePoint;
import models.ZebRoute;
import ninja.Context;
import ninja.FilterWith;
import ninja.Result;
import ninja.Results;
import ninja.params.PathParam;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import services.filters.AdminFilter;
import services.filters.DriverFilter;
import services.helpers.Constants;
import services.helpers.Pagination;
import services.helpers.Utils;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import dao.GenericDao;
import dao.LookupGroupDao;
import dao.MemberDao;
import dto.DailyScheduleDto;
import dto.ZebRouteDto;

@Singleton
@FilterWith({DriverFilter.class, AdminFilter.class})
public class AdminController{
	
	@Inject
	MemberDao memberDao;
	
	@Inject
	LookupGroupDao lookupGroupDao;
	
	@Inject
	GenericDao dao;

	private Log log = LogFactory.getLog(AdminController.class);    

	public Result adminFront() {
		return Results.html();
	}
	
	public Result partnerList(Context context) {
		String page = context.getParameter("page");
		
		List<Partner> partners = null;
		String pagination="";
		int number = 0;
		try {
			int pageSize = Constants.PageSize;
			Integer paged = Integer
					.valueOf(page != null ? page : "1");
			number = Utils.sequenceForList(paged, pageSize);
			partners = dao.findByPage(Partner.class, paged, pageSize);
			int total = dao.countAll(Partner.class);
			pagination = Pagination.generatePage(total, paged, pageSize);
		} catch (Exception e) {
			// TODO: handle exception
		}
		Result result = Results.html();
		result.render("entityList", partners);
		result.render("pagination", pagination);
		result.render("number", number);
		//System.out.println(pagination);
		return result;
	}
	
	public Result memberList(Context context) {
		String page = context.getParameter("page");
		Map<String, Map> paging = new HashMap<String, Map>();
		
		List<Member> members = null;
		String pagination="";
		int number = 0;
		try {
			int pageSize = Constants.PageSize;
			Integer paged = Integer
					.valueOf(page != null ? page : "1");
			number = Utils.sequenceForList(paged, pageSize);
			members = dao.findByPage(Member.class, paged, pageSize);
			int total = dao.countAll(Member.class);
			pagination = Pagination.generatePage(total, paged, pageSize);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("================================= ++++ "+members.get(0).getUsername());
		
		Result result = Results.html();
		result.render("listMember", members);
		result.render("pagination", pagination);
		result.render("number", number);
		//System.out.println(pagination);
		return result;
	}
	
	public Result memberView(@PathParam("id") Long id) {
		
		Member member = dao.find(Member.class, id);
				
		Map<String, Map> dropdown = new HashMap<String, Map>();
		HashMap<String, String> genderLookup = new HashMap<String, String>();
		genderLookup.put("L", "Laki-laki");
		genderLookup.put("P", "Perempuan");
		
		dropdown.put("gender", genderLookup);
		
		Result result = Results.html()
				.render("member", member)
				.render("dropdown", dropdown);
	    return result;
		
	}

	public Result updateMember(Context context){
		
		Member member = new Member();
		Address addr = new Address();
		Set<Address> addrs = new HashSet<Address>();
		
		MemberSocmed socmed = new MemberSocmed();
		Set<MemberSocmed> socmeds = new HashSet<MemberSocmed>();
		
		Contact contact = new Contact();
		Set<Contact> contacts = new HashSet<Contact>();
		
		try {      
	        
			DateFormat format = new SimpleDateFormat("EEE MMMM dd yyyy", Locale.ENGLISH);
			
			FileItemIterator fileItemIterator = context.getFileItemIterator();
			
			
			while (fileItemIterator.hasNext()) {
				FileItemStream item = fileItemIterator.next();
				String name = item.getFieldName();
				InputStream inputStream = item.openStream();
				//String contentType = item.getContentType();

				if (item.isFormField()) {
					String content = Streams.asString(inputStream);
					switch (name) {
						case "id"			: member = dao.find(Member.class, Long.parseLong(content)); break;
						case "fullname"		: member.setFullname(content);break;
						case "snsId"		: member.setFbid(content);break;
						case "firstname"	: member.setFirstname(content);break;
						case "lastname"		: member.setLastname(content);break;
						case "birthdate1"	: member.setBirthdate(format.parse(content));break;
						case "gender"		: member.setGender(content);break;
						case "fblogin"		: member.setFblogin(Boolean.parseBoolean(content));break;
					
						case "addresstype"	: addr.setAddresstype(content);break;
						case "addressline1"	: addr.setAddressline1(content);break;
						case "addressline2"	: addr.setAddressline2(content);break;
						case "district"		: addr.setDistrict(content);break;
						case "city"			: addr.setCity(content);break;
						case "province"		: addr.setProvince(content);break;
						case "country"		: addr.setCountry(content);break;
						case "postcode"		: addr.setPostcode(content);break;
					
						case "socmedno"		: socmed.setSocmedno(content);break;
						case "resfield1"	: socmed.setResfield1(content);break;
						case "resfield2"	: socmed.setResfield2(content);break;
						case "resfield3"	: socmed.setResfield3(content);break;
						case "resfield4"	: socmed.setResfield4(content);break;
					
						case "contacttype"	: contact.setContacttype(content);break;
						case "detail"		: contact.setDetail(content);break;
						case "channel"		: contact.setChannel(content);break;
					} //end switch
				} else{
					//set image path
					String imageName = item.getName();
					if(imageName != null && imageName != ""){

						String path = Constants.PATH+"/images/";
						File fileSaveDir = new File(path);
						if (!fileSaveDir.exists()) {
							fileSaveDir.mkdir();
						}

						path = path+"/member/";
						fileSaveDir = new File(path);
						if (!fileSaveDir.exists()) {
							fileSaveDir.mkdir();
						}

						fileSaveDir = new File(path+member.getId()+"_version.png");	

						OutputStream output = new FileOutputStream(fileSaveDir);
						Streams.copy(inputStream, output, true);

						inputStream.close();
						output.close();

						member.setImagepath("/images/member/"+member.getId()+"_version.png");
					}else{
						member.setImagepath(member.getImagepath());
					}
				} //end if item.formField
			}
			
			addr.setMember(member);
			addrs.add(addr);
			member.setAddress(addrs);
			
			socmed.setMember(member);
			socmeds.add(socmed);
			member.setSocmed(socmeds);
			
			contact.setMember(member);
			contacts.add(contact);
			member.setContact(contacts);
			
			dao.merge(member);
			
		} catch (RollbackException re) {
			Set<ConstraintViolation<?>> cv = ((ConstraintViolationException) re.getCause()).getConstraintViolations();
//			cv.iterator().next().getMessage()
			context.getFlashScope().error(cv.iterator().next().getPropertyPath()+".error");
			return Results.redirect("/admin/member/"+member.getId());
		}catch (Exception e) {
			e.printStackTrace();
			context.getFlashScope().error("error");
			return Results.redirect("/admin/member/"+member.getId());
		}

		context.getFlashScope().success("member.update.success");
		return Results.redirect("/admin/member/"+member.getId());

	}
	
	/*public Result uploadImage(Context context) throws Exception {
		
		Member member = new Member();
		
		if(context.isMultipart()){

			FileItemIterator fileItemIterator = context.getFileItemIterator();

			while (fileItemIterator.hasNext()) {
				FileItemStream item = fileItemIterator.next();
				String name = item.getFieldName();
				InputStream stream = item.openStream();
				String contentType = item.getContentType();
				
				if (item.isFormField()) {
					if(name.equalsIgnoreCase("id")){
						Member mem = dao.find(Member.class, Long.parseLong(Streams.asString(stream)));
						member = mem;
					}
					
				} else {
					// process file as input stream
					try {
						byte[] bytes = IOUtils.toByteArray(stream);
						stream.read(bytes);
						member.setImage(bytes);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		
		dao.merge(member);
		context.getFlashScope().success("success.image.upload");
		return Results.redirect("/admin/member/"+member.getId());
	}*/
	
	public Result retriveImage(@PathParam("id") Long id, Context context) throws Exception {
		Member member = new Member();
		member = dao.find(Member.class, id);
		
		byte[] bImage = new byte[1024];
		bImage = member.getImage();
		
		Result resultImg = Results.contentType("image/jpeg").renderRaw(bImage);
		return resultImg;
	}
	
	public Result uploadImage(Context context) throws Exception {
		
		Member member = new Member();

		if(context.isMultipart()){

			FileItemIterator fileItemIterator = context.getFileItemIterator();

			while (fileItemIterator.hasNext()) {
				FileItemStream item = fileItemIterator.next();
				String name = item.getFieldName();
				InputStream inputStream = item.openStream();
				//String contentType = item.getContentType();
				
				if (item.isFormField()) {
					if(name.equalsIgnoreCase("id")){
						Member mem = dao.find(Member.class, Long.parseLong(Streams.asString(inputStream)));
						member = mem;
					}
					
				} else {
					// process file as input stream
					try {
						String imageName = item.getName();
						if(imageName != null && imageName != ""){
							
							String path = Constants.PATH+"/images/";
							File fileSaveDir = new File(path);
							if (!fileSaveDir.exists()) {
								fileSaveDir.mkdir();
						    }
							
							path = path+"/profile/";
							fileSaveDir = new File(path);
							if (!fileSaveDir.exists()) {
								fileSaveDir.mkdir();
						    }

							fileSaveDir = new File(path+member.getId()+"_version.png");	
			
							OutputStream output = new FileOutputStream(fileSaveDir);
							Streams.copy(inputStream, output, true);
											
							inputStream.close();
							output.close();
							
							member.setImagepath("/images/profile/"+member.getId()+"_version.png");
						}else{
							member.setImagepath("");
						}

					} catch (Exception e) {
						e.printStackTrace();
					}


				}
			}
		}
		
		dao.merge(member);
		context.getFlashScope().success("success.image.upload");
		return Results.redirect("/admin/member/"+member.getId());
	}

	/*public Result checkAvailableLookupGroup(@Param("code") String code) {
        return Results.json().render(code);
    }
    */
    
    public Result categoryList() {
    	return Results.html();
    }
    
	public Result routeList(Context context) {
		
		List<ZebRoute> zebroutes = null;
		try {
			zebroutes = dao.findAll(ZebRoute.class);
		} catch (Exception e) {
			// TODO: handle exception
		}
		Result result = Results.html();
		result.render("entityList", zebroutes);
		return result;
	}
	
	public Result routeView(@PathParam("id") Long id) {

		ZebRoute zebRoute = dao.find(ZebRoute.class, id);

		if (zebRoute == null || zebRoute.getId() == null){
			zebRoute = new ZebRoute(id);
		}
		
		if (zebRoute.getRoutePoint().size() < 6) {
			for (int i = zebRoute.getRoutePoint().size(); i < 6; i++) {
				zebRoute.getRoutePoint().add(new RoutePoint());
			}
		}

//		Result result = Results.html().render("zebroute", zebRoute);
//		return result;

		return Results.html().render("zebroute", zebRoute);
	}
	
	public Result routeSave(Context context, ZebRouteDto zebRouteDto) {
		
		boolean isNew = false;
		//List<ZebRoute> checkAvailble = dao.findByProperty(ZebRoute.class, "code", zebRouteDto.code);
		try {
			ZebRoute zebRoute = dao.find(ZebRoute.class, zebRouteDto.id);

			if (zebRoute == null) {
				isNew = true;
				zebRoute = new ZebRoute();	
			}
			Long saveId = new Long(0);

			zebRoute.setCode(zebRouteDto.code);
			zebRoute.setDescription(zebRouteDto.description);
			zebRoute.setFromname(zebRouteDto.fromname);
			zebRoute.setFrompoint(zebRouteDto.frompoint);
			zebRoute.setFromdisplay(zebRouteDto.fromdisplay);
			zebRoute.setName(zebRouteDto.name);
			zebRoute.setPrice(zebRouteDto.price);
			zebRoute.setToname(zebRouteDto.toname);
			zebRoute.setTodisplay(zebRouteDto.todisplay);
			zebRoute.setTopoint(zebRouteDto.topoint);
			
			List<RoutePoint> routePoints = new ArrayList<RoutePoint>();
			
			for(int i=0;i<zebRouteDto.locName.length;i++) {
				if (!"".equals(zebRouteDto.locName[i])) {
					RoutePoint routePoint = new RoutePoint();
					routePoint.setZebRoute(zebRoute);
					routePoint.setLocOrder(i);
					routePoint.setLocName(zebRouteDto.locName[i]);
					routePoint.setLocDisplay(zebRouteDto.locDisplay[i]);
					routePoint.setLocPoint(zebRouteDto.locPoint[i]);
					routePoints.add(routePoint);
				}
			}
			
			zebRoute.setRoutePoint(routePoints);
			
			if (isNew) {
				saveId = dao.save(zebRoute);
			} else {
				dao.merge(zebRoute);
			}
			context.getFlashScope().success("successfully.added");
	    	return Results.redirect("/admin/route");
		} catch (Exception e) {
			context.getFlashScope().error("failed");
	    	return Results.redirect("/admin/route");
		}
	}
	
	public Result scheduleView(@PathParam("id") Long id) {

		ZebRoute zebRoute = dao.find(ZebRoute.class, id);
		List<DailySchedule> dailyScheduleList = dao.scheduleView(DailySchedule.class, "route_id", id);
		return Results.html().render("zebroute", zebRoute).render("dailyScheduleList", dailyScheduleList);
	}

	public Result scheduleSave(Context context, DailyScheduleDto dailyScheduleDto) {

		ZebRoute zebRoute = dao.find(ZebRoute.class, dailyScheduleDto.routeid);

//		try {
			SimpleDateFormat dateParser = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat timeParser = new SimpleDateFormat("HH:mm");
			
			for(int i=0;i<dailyScheduleDto.tripdate.length;i++) {
				if ("new".equals(dailyScheduleDto.status[i])) {
					DailySchedule dailySchedule = new DailySchedule();
					dailySchedule.setZebRoute(zebRoute);
					try {
						dailySchedule.setTripdate(new java.sql.Date(dateParser.parse(dailyScheduleDto.tripdate[i]).getTime()));
						dailySchedule.setTriptime(new java.sql.Time(timeParser.parse(dailyScheduleDto.triptime[i]).getTime()));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					dailySchedule.setAllocation(dailyScheduleDto.allocation[i]);
					dao.save(dailySchedule);
				} else if ("del".equals(dailyScheduleDto.status[i])) {
					dao.delete(DailySchedule.class, dailyScheduleDto.scheduleid[i]);
				}
			}
			context.getFlashScope().success("successfully.added");
			return Results.redirect("/admin/schedule/"+dailyScheduleDto.routeid);
			
//		} catch (Exception e) {
//			context.getFlashScope().error("failed");
//			return Results.redirect("/admin/trip/"+dailyTripDto.routeid);
//		}
	}
	
	public Result dailyScheduleView(Context context) {

		List<DailyScheduleView> dailySchedules = null;
		try{
			dailySchedules = dao.findDailyScheduleView(DailyScheduleView.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Result result = Results.html();
		result.render("entityList", dailySchedules);
		return result;
	}

	public Result dailyScheduleDetail(@PathParam("id") Long id, Context context) {

		List<DailyScheduleView> dailySchedules = null;
		try{
			dailySchedules = dao.findDailyScheduleView(DailyScheduleView.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Result result = Results.html();
		result.render("entityList", dailySchedules);
		return result;
	}

	@FilterWith(AdminFilter.class)
	public Result routeUploadImg(Context context) {

		ZebRoute zebRoute = null;
		
		if (context.isMultipart()) {
			try{
				FileItemIterator fileItemIterator = context.getFileItemIterator();
				while (fileItemIterator.hasNext()) {
					FileItemStream item = fileItemIterator.next();
					String name = item.getFieldName();
					InputStream inputStream = item.openStream();

					if (item.isFormField()) {
						String content = Streams.asString(inputStream);
						switch (name) {
							case "id":
								zebRoute = dao.find(ZebRoute.class, Long.parseLong(content));
								break;
						} //end switch
					} else{
						//set image path
						String imageName = item.getName();
						if(imageName != null && imageName != ""){

							String path = Constants.PATH+"/images/";
							File fileSaveDir = new File(path);
							if (!fileSaveDir.exists()) {
								fileSaveDir.mkdir();
							}
					
							path = path+"/route/";
							fileSaveDir = new File(path);
							if (!fileSaveDir.exists()) {
								fileSaveDir.mkdir();
							}
					
							//generate random for file name
							String fileNm = zebRoute.getId()+zebRoute.getCode();
							fileSaveDir = new File(path+fileNm+".png");	
					
							OutputStream output = new FileOutputStream(fileSaveDir);
							Streams.copy(inputStream, output, true);
					
							inputStream.close();
							output.close();
					
							zebRoute.setImagepath("/images/route/"+fileNm+".png");
							dao.merge(zebRoute);
							
						}//end imageName
					} //end if item.formField
				} // end while fileItemIterator
			} catch (Exception e) {
				e.printStackTrace();
				return Results.text().render("Data not saved.");   
			}
		}
		return Results.text().render("Upload image saved");   
	}
}

