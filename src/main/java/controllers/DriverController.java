/**
 * Copyright (C) 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.common.collect.Lists;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import dao.DriverDao;
import dto.BookedDto;
import dto.DriverScheduleDto;
import dto.ZebvanDto;
import models.BookTrip;
import models.DailySchedule;
import models.DailyScheduleView;
import models.Driver;
import models.Lookup;
import models.Member;
import models.Zebvan;
import ninja.Context;
import ninja.FilterWith;
import ninja.Result;
import ninja.Results;
import ninja.params.PathParam;
import ninja.scheduler.Schedule;
import ninja.validation.JSR303Validation;
import ninja.validation.Validation;
import services.filters.AdminFilter;
import services.filters.DriverFilter;
import services.helpers.Constants;

@Singleton
public class DriverController{
	
	@Inject
	DriverDao dao;

	private Log log = LogFactory.getLog(DriverController.class);
	
	@FilterWith(AdminFilter.class)
	public Result vehicleView(@PathParam("id") Long id,Context context) {
		
		Zebvan zebvan = null;
		try {
			zebvan = dao.find(Zebvan.class, id);
			if (zebvan == null || zebvan.getId() == null){
				zebvan = new Zebvan();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Result result = Results.html();
		result.render("entity", zebvan);
		return result;
	}

	@FilterWith(AdminFilter.class)
	public Result vehicleSave(Context context) {
		
		boolean isNew = false;
		long saveId = 0;
		Zebvan zebvan = null;
		ZebvanDto zebvanDto = new ZebvanDto();
		SimpleDateFormat dateParser = new SimpleDateFormat("dd-MM-yyyy");

		if (context.isMultipart()) {
			try{
				FileItemIterator fileItemIterator = context.getFileItemIterator();
				while (fileItemIterator.hasNext()) {
					FileItemStream item = fileItemIterator.next();
					String name = item.getFieldName();
					InputStream inputStream = item.openStream();
					//String contentType = item.getContentType();

					if (item.isFormField()) {
						String content = Streams.asString(inputStream);
						switch (name) {
							case "id"			: zebvanDto.setId(Long.parseLong(content)); break;
							case "driver_id"	: zebvanDto.setDriverId(Long.parseLong(content)); break;
							case "plateNo"		: zebvanDto.setPlateNo(content); break;
							case "make"			: zebvanDto.setMake(content); break;
							case "model"		: zebvanDto.setModel(content); break;
							case "color"		: zebvanDto.setColor(content); break;
							case "year"			: zebvanDto.setYear(Integer.parseInt(content)); break;
							case "seatAvailable": zebvanDto.setSeatAvailable(Integer.parseInt(content)); break;
							case "stnkno"		: zebvanDto.setStnkNo(content); break;
							case "stnkexp"		: zebvanDto.setStnkExp(new java.sql.Date(dateParser.parse(content).getTime())); break;
							case "kirno"		: zebvanDto.setKirNo(content); break;
							case "kirexp"		: zebvanDto.setKirExp(new java.sql.Date(dateParser.parse(content).getTime())); break;
							case "description"	: zebvanDto.setDescription(content); break;
						} //end switch
					} else{
						//set image path
						String imageName = item.getName();
						if(imageName != null && imageName != ""){

							String path = Constants.PATH+"/images/";
							File fileSaveDir = new File(path);
							if (!fileSaveDir.exists()) {
								fileSaveDir.mkdir();
							}

							path = path+"/vehicle/";
							fileSaveDir = new File(path);
							if (!fileSaveDir.exists()) {
								fileSaveDir.mkdir();
							}

							//generate random for file name
							fileSaveDir = new File(path+imageName+"_version.png");	

							OutputStream output = new FileOutputStream(fileSaveDir);
							Streams.copy(inputStream, output, true);

							inputStream.close();
							output.close();

							zebvanDto.setImagePath("/images/vehicle/"+imageName+"_version.png");
						}//end imageName
					} //end if item.formField
				} // end while fileItemIterator

				Driver driver = dao.find(Driver.class, zebvanDto.getDriverId());
	   			if (zebvanDto.getId() == null) {
					isNew = true;
					zebvan = new Zebvan();	
					zebvan.setDriver(driver);
				} else {
					zebvan = dao.find(Zebvan.class, zebvanDto.getId());
				}

//   			zebvan.setColor(zebvanDto.color);
//   			zebvan.setImagePath(zebvanDto.image);
//   			zebvan.setKirExp(new java.sql.Date(dateParser.parse(zebvanDto.kirexp).getTime()));
//   			zebvan.setKirNo(zebvanDto.kirno);
//   			zebvan.setMake(zebvanDto.make);
//   			zebvan.setModel(zebvanDto.model);
//   			zebvan.setPlateNo(zebvanDto.plateNo);
//   			zebvan.setSeatAvailable(Integer.parseInt(zebvanDto.seatAvailable));
//   			zebvan.setStnkExp(new java.sql.Date(dateParser.parse(zebvanDto.stnkexp).getTime()));
//   			zebvan.setStnkNo(zebvanDto.stnkno);
//   			zebvan.setYear(Integer.parseInt(zebvanDto.year));
//   			zebvan.setName(zebvanDto.name);
//   			zebvan.setCode(zebvanDto.code);
//   			zebvan.setDescription(zebvanDto.description);

				BeanUtils.copyProperties(zebvan, zebvanDto);

				if (isNew) {
					saveId = dao.save(zebvan);
				} else {
					dao.merge(zebvan);
				}
				
     		} catch (Exception e) {
    			e.printStackTrace();
				return Results.text().render("Data not saved.");   
    		}
		} //end context.isMultipart
		return Results.text().render("Data saved: " + saveId);   
	}

	@FilterWith(AdminFilter.class)
	public Result driverList(Context context) {
		
		List<Driver> drivers = null;
		try {
			drivers = dao.findAll(Driver.class);
		} catch (Exception e) {
			// TODO: handle exception
		}
		Result result = Results.html();
		result.render("entityList", drivers);
		return result;
	}

	@FilterWith(AdminFilter.class)
	public Result driverView(@PathParam("id") Long id) {

		Driver driver = null;
		List<DailySchedule> dailySchedules = new ArrayList<DailySchedule>();
		List<DriverScheduleDto> driverScheduleList = new ArrayList<DriverScheduleDto>();
		Map<String, Map> dropdown = new HashMap<String, Map>();
		boolean isNew = false;

		try {
			driver = dao.find(Driver.class, id);
			if (driver == null || driver.getId() == null){
				driver = new Driver();
				isNew = true;
			}

			HashMap<String, String> zebvanLookup = new HashMap<String, String>();
			if (!isNew) {
	    		List< Zebvan> zebvanList = dao.findByParentId(Zebvan.class, "driver_id", driver.getId());
	    		for(Zebvan van :zebvanList){
	    			zebvanLookup.put(Long.toString(van.getId()), van.getPlateNo() + "-" + van.getCode());
	    		}
			} //end !isNew
			dropdown.put("zebvan", zebvanLookup);
			
			Map<String, Object> paramMap = new HashMap<String,Object>();
			paramMap.put("this_date","Y");
			dailySchedules = (List<DailySchedule>) dao.findAvailScheduleByDate(DailySchedule.class);
	
			//loop on dailySR
			Date currDate = new Date();
			currDate = new SimpleDateFormat("dd/MM/yyyy").parse("01/01/2016");

			DriverScheduleDto drivSch = new DriverScheduleDto();
			drivSch.routeType = new HashMap<String, Map>();
			HashMap<String, String> morningRoute = new HashMap<String, String>();
			HashMap<String, String> eveningRoute = new HashMap<String, String>();
			int counter = 0;

			for (DailySchedule ds : dailySchedules) {
				if (currDate.compareTo(ds.getTripdate()) == 0) { //same date
					if(ds.getScheduleType().equals("A")) {
						morningRoute.put(ds.getId().toString(), ds.getZebRoute().getCode()+"-"+ds.getZebRoute().getName()+" ("+ ds.getTriptime() +")");
					} else {
						eveningRoute.put(ds.getId().toString(), ds.getZebRoute().getCode()+"-"+ds.getZebRoute().getName()+" ("+ ds.getTriptime() +")");
					}
				} else { //diff date
					if (counter > 0) {
						drivSch.routeType.put("A", morningRoute);
						drivSch.routeType.put("B", eveningRoute);
						driverScheduleList.add(drivSch);

						drivSch = new DriverScheduleDto();
						drivSch.routeType = new HashMap<String, Map>();
						morningRoute = new HashMap<String, String>();
						eveningRoute = new HashMap<String, String>();
					}
					
					currDate = ds.getTripdate();
					drivSch.scheduledate = ds.getTripdate();

					if(ds.getScheduleType().equals("A")) {
						morningRoute.put(ds.getId().toString(), ds.getZebRoute().getCode()+"-"+ds.getZebRoute().getName()+" ("+ ds.getTriptime() +")");
					} else {
						eveningRoute.put(ds.getId().toString(), ds.getZebRoute().getCode()+"-"+ds.getZebRoute().getName()+" ("+ ds.getTriptime() +")");
					}
				}
				counter++;
			}
			if (morningRoute != null) drivSch.routeType.put("A", morningRoute);
			if (eveningRoute != null) drivSch.routeType.put("B", eveningRoute);
			if (counter>0) driverScheduleList.add(drivSch);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return Results.html()
					.render("driver", driver)
					.render("dailyScheduleList", driverScheduleList)
					.render("dropdown", dropdown);
	}
	
    public Result driverVans(@PathParam("id") Long driverId) {
    	List<Zebvan> entityList = dao.findVehicleByDriverId(driverId);
		return Results.json().render("zebvan", entityList);
    }

	@FilterWith(AdminFilter.class)
	public Result zebvanList(Context context) {
		
		List<Zebvan> zebvans = null;
		try {
			zebvans = dao.findAll(Zebvan.class);
		} catch (Exception e) {
			// TODO: handle exception
		}
		Result result = Results.html();
		result.render("entityList", zebvans);
		return result;
	}

    @FilterWith(AdminFilter.class)
	public Result driverSave(Context context, 
							@JSR303Validation DriverScheduleDto driverScheduleDto,
							Validation validation) {

		if (validation.hasViolations()) {
            context.getFlashScope().error("error.correct.field");
            return Results.redirect("/admin/driver");
        } else {
    	    final String MEMNO = "memNo";

    		String memId = (null == context.getSession().get(MEMNO))?"0":context.getSession().get(MEMNO);
    		Member member = dao.find(Member.class, Long.parseLong(memId));
    		
    		Zebvan defaultvan = dao.find(Zebvan.class, driverScheduleDto.defaultvan);

    		boolean isNew = false;
    		try {
    			Driver driver = null;
    			if (driverScheduleDto.driverid == null || driverScheduleDto.driverid == 99999999) {
    				isNew = true;
    				driver = new Driver();	
    				driver.setMember(member);
    			} else {
        			driver = dao.find(Driver.class, driverScheduleDto.driverid);
    				driver.getScheduleIds().clear();
    				driver = dao.merge(driver);
    			}
    			Long saveId = new Long(0);
    			
    			driver.setKtpNo(driverScheduleDto.ktpno);
    			driver.setName(driverScheduleDto.drivername);
    			driver.setSimNo(driverScheduleDto.simno);
    			driver.setPhoneNo(driverScheduleDto.driverphone);
    			driver.setDeviceKey(driverScheduleDto.devicekey);
    			driver.setDefaultvan(defaultvan);
    			
    			Long[] validTripId = null;
    			if (driverScheduleDto.tripid != null) {
	    			for (int i=0;i<driverScheduleDto.tripid.length;i++) {
	    				if (driverScheduleDto.tripid[i] > 0) {
	    					driver.getScheduleIds().add(driverScheduleDto.tripid[i]);
	    					 //ArrayUtils.add(validTripId, driverScheduleDto.tripid[i]);
	    				}
	    			}
//	    			if (validTripId != null) {
//	        			driver.setScheduleIds(Lists.newArrayList(validTripId));
//	    			}
    			}
    			if (isNew) {
    				saveId = dao.save(driver);
    			} else {
    				dao.merge(driver);
    			}
    		} catch (Exception e) {
    			e.printStackTrace();
    		}
        }
		return Results.text().render("Data successfully saved.");
	}
    
    //allocateList
	@FilterWith(AdminFilter.class)
	public Result allocateList(Context context) {

		List<DailyScheduleView> dailySchedules = null;
		try{
			dailySchedules = dao.findDailyScheduleView(DailyScheduleView.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Result result = Results.html();
		result.render("entityList", dailySchedules);
		return result;
	}

	//dailyScheduleView
	@FilterWith(AdminFilter.class)
	public Result allocateView(@PathParam("id") Long id) {
		
		Map<String, Object> paramMap = new HashMap<String,Object>();
		List<BookTrip> btBooked = null;
		List<BookTrip> btAllocated = null;
		List<ZebvanDto> vanList = null;
		DailySchedule schedule = new DailySchedule();
		
		try {
			paramMap.put("trip_id",id);
			schedule = dao.findScheduleWithRoute(DailySchedule.class, id);
			btBooked = dao.findAllWithMultiParams("bookTripBooked", BookTrip.class, paramMap);
			btAllocated = dao.findAllWithMultiParams("bookTripAllocated", BookTrip.class, paramMap);
			vanList = dao.findZebvanBasedOnTripSelected(id);
			ArrayList<BookTrip> bookTrips = new ArrayList<BookTrip>(); 
			
			for (ZebvanDto zvDto : vanList) {
				Long vanId = zvDto.getId();
				for (BookTrip bt : btAllocated) {
					if (vanId == bt.getZebvan().getId()) {
						bookTrips.add(bt);
					} //if
				} //for btAllocated
				zvDto.setBooktrips(bookTrips);
			}//for vanList
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		Result result = Results.html();
		result.render("schedule", schedule).render("bookList", btBooked).render("vanList",vanList);
		return result;
	} //allocateView
	
    //allocateList
	@FilterWith(AdminFilter.class)
	public Result allocateSave(Context context, BookedDto bookedDto) {

		try{
			BookTrip bookTrip = new BookTrip();
			//loop for allocated
			if (bookedDto.allocated != null) {
				for (String valueStr : bookedDto.allocated) {
					String[] formData = valueStr.split("\\|");
					// bookingId, vanId, driverId
							
					bookTrip = dao.find(BookTrip.class, Long.parseLong(formData[0]));
					bookTrip.setZebvan(dao.find(Zebvan.class,Long.parseLong(formData[1])));
					bookTrip.setDriver(dao.find(Driver.class,Long.parseLong(formData[2])));
					bookTrip.setStatus("Allocated");
					dao.merge(bookTrip);
				}//end for
			} //end if allocated
			
			if (bookedDto.booked != null) {
				for (String valueStr : bookedDto.booked) {
					String[] formData = valueStr.split("\\|");
					// bookingId
					bookTrip = dao.find(BookTrip.class, Long.parseLong(formData[0]));
					bookTrip.setDriver(null);
					bookTrip.setZebvan(null);
					bookTrip.setStatus("Booked");
					dao.merge(bookTrip);
				} //end for
			} //end if booked

		} catch (Exception e) {
			e.printStackTrace();
			context.getFlashScope().error("failed");
			return Results.redirect("/admin/allocate/" + bookedDto.tripId);
		}
		context.getFlashScope().success("successfully.added");
		return Results.redirect("/admin/allocate/" + bookedDto.tripId);
	}
	
//	triplist copy from rideAvailableList
	@FilterWith(DriverFilter.class)
	public Result driverRideList(Context context){
	    final String MEMNO = "memNo";
	    List<BookTrip> booktrips = null;
	    try {
			Map<String, Object> paramMap = new HashMap<String,Object>();

			String memId = context.getSession().get(MEMNO);
			if (null == memId || "".equals(memId)){
				throw new Exception();
			}
			Member member = dao.find(Member.class, Long.parseLong(memId));
			List<Driver> drivers = dao.findByParentId(Driver.class, "member_id", member.getId());
			
			paramMap.put("this_date","Y");
			paramMap.put("driver_id",drivers.get(0).getId());
			booktrips = dao.findAllWithMultiParams("bookTripByDriver", BookTrip.class, paramMap);

		} catch (Exception e) {
			e.printStackTrace();
		}

		Result result = Results.html();
		result.render("tripList", booktrips);
        return result;
    }
}


