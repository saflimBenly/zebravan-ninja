/**
 * Copyright (C) 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import dao.GenericDao;
import models.EnumOrder;
import models.Lookup;
import models.LookupGroup;
import ninja.Context;
import ninja.FilterWith;
import ninja.Result;
import ninja.Results;
import ninja.params.Param;
import ninja.params.Params;
import ninja.session.FlashScope;
import services.filters.AdminFilter;

@Singleton
public class LookupController{
	
	@Inject
	GenericDao dao;

	private Log log = LogFactory.getLog(LookupController.class);

	/**
	 * aad
	 * @return
	 */
	@FilterWith(AdminFilter.class)
    public Result lookupGroupList(
    		@Param("pageNumber") int pageNumber ,
    		@Param("pageIndex") int pageIndex,
    		@Param("pageSize") int pageSize) {
    	//List<LookupGroup> lookupGroups = dao.findAll(LookupGroup.class);
    	pageIndex = (pageIndex!=0)?pageIndex:0;
    	pageSize = (pageSize!=0)?pageSize:5;
    	pageNumber  = (pageNumber!=0)?pageNumber:1;
    	log.debug("=========pageIndex====="+pageIndex);
    	log.debug("=========pageSize======"+pageSize);
    	
    	Result result = Results.html();
    	result.render("listLookupGroup", dao.findByPage(LookupGroup.class, pageIndex, pageSize));
    	
    	//paging
    	/* * pageNumber -> The current page number
    	 * pageSize -> The number of items in each page
    	 * pagesAvailable -> The total number of pages*/
    	
    	HashMap<String, Object> paginationData = new HashMap<String, Object>();
    	paginationData.put("pagesAvailable", dao.findAll(LookupGroup.class).size());
    	paginationData.put("pageIndex",pageIndex);
    	paginationData.put("pageSize",pageSize);
    	paginationData.put("pageNumber",pageNumber);
    	result.render("paginationData", paginationData);
    	
    	log.debug("=========pageIndex====="+pageIndex);
    	log.debug("=========pageSize======"+pageSize);
    	log.debug("=========TOTAL========="+dao.findAll(LookupGroup.class).size());
		return result;
    }
	    
	@FilterWith(AdminFilter.class)
    public Result addLookupGroup(@Param("code") String code,@Param("description") String description, Context context, FlashScope flashScope) {
    		LookupGroup lookupGroup = new LookupGroup(code, description);
    		log.debug("=========================================start==============================================");
    		List<LookupGroup> checkAvailbleData = dao.findByProperty(LookupGroup.class, "code", code);
    		if(checkAvailbleData.size()==0){//save
    			dao.save(lookupGroup);
    			context.getFlashScope().success("successfully.added");
    		}else//fail
    			context.getFlashScope().error("failed");
    		log.debug("=========================================success==============================================");
			return Results.redirect("/admin/lookupGroupList");
	}
    
	@FilterWith(AdminFilter.class)
    public Result updateLookupGroup(@Param("id") Long id,@Param("code") String code,@Param("description") String description, Context context, FlashScope flashScope) {
		LookupGroup dataFromTable = dao.find(LookupGroup.class, id);
		log.debug("=========================================start==============================================");
		List<LookupGroup> checkAvailbleData = dao.findByProperty(LookupGroup.class, "code", code);
		if(checkAvailbleData.size()==0||
				checkAvailbleData.size()>0 && checkAvailbleData.get(0).getCode().equals(code) && checkAvailbleData.get(0).getId() == id){//save
			LookupGroup update =  checkAvailbleData.get(0);
			update.setCode(code);
			update.setDescription(description);
	        dao.merge(update);
			context.getFlashScope().success("successfully.edited");
		}else//fail
			context.getFlashScope().error("failed");
		log.debug("=========================================success==============================================");
		return Results.redirect("/admin/lookupGroupList");
    }
    
	@FilterWith(AdminFilter.class)
    public Result deleteLookupGroup(@Params("id") String[] ids,Context context, FlashScope flashScope) {
    	for(String id : ids){
    		//delete data on lookup table
    		List< Lookup> lookupList = dao.findByProperty(Lookup.class, "lookupgroup_id", Long.parseLong(id));
    		for(Lookup data :lookupList){
    			dao.delete(Lookup.class, data.getId());
    		}
    		
    		//delete data on lookup group table
    		dao.delete(LookupGroup.class, Long.parseLong(id));
    		context.getFlashScope().success("successfully.deleted");
    	}
    	return Results.redirect("/admin/lookupGroupList");
    }
    
	@FilterWith(AdminFilter.class)
    public Result deleteLookup(@Params("id") String[] ids,Context context, FlashScope flashScope) {
    	for(String id : ids){
    		//delete data on lookup  table
    		List<Lookup> checkAvailbleDataLookup = dao.findByProperty(Lookup.class, "id", id);
    		List<LookupGroup> checkAvailbleDataLookupGroup =  dao.findByProperty(LookupGroup.class, "code", checkAvailbleDataLookup.get(0).getCode());
    		log.debug("===================="+Long.parseLong(id)+"=====================DeLeTe==============================================");
    		dao.delete(Lookup.class, Long.parseLong(id));
    		context.getFlashScope().success("successfully.deleted");
    	}
    	return Results.redirect("/admin/lookupList");
    }
    
	@FilterWith(AdminFilter.class)
    public Result addLookup(@Param("code") String code,@Param("name") String name,@Param("description") String description, Context context, FlashScope flashScope){
    	log.debug("=========================================start==============================================");
		List<Lookup> checkAvailbleDataLookup = dao.findByProperty(Lookup.class, "code", code);
		
		boolean allow=true;
		long id =0;
		for(Lookup data : checkAvailbleDataLookup){
			if(data.getName().equals(name)){
				allow=false;
			}
		}
		if(allow){
			LookupGroup data = dao.findByProperty(LookupGroup.class, "code", code).get(0); 
			Lookup lookup = new Lookup(data.getId(), code, name, description);
			lookup.setLookupGroup(data);
			dao.save(lookup);
			context.getFlashScope().success("successfully.added");
		}else//fail
			context.getFlashScope().error("failed");
		log.debug("=========================================success==============================================");
    	
    	return Results.redirect("/admin/lookupList");
    }

    @FilterWith(AdminFilter.class)
    public Result updateLookup(@Param("id") Long id,@Param("code") String code,@Param("name") String name,@Param("description") String description,@Param("useYN") String useYN, Context context, FlashScope flashScope) {
		Lookup dataFromTable = dao.find(Lookup.class, id);
		log.debug("=========================================start==============================================");
		List<Lookup> checkAvailbleData = dao.findByProperty(Lookup.class, "name", name);
		if(checkAvailbleData.size()==0||
				checkAvailbleData.size()>0 && checkAvailbleData.get(0).getName().equals(name) && checkAvailbleData.get(0).getId() == id){//save
			Lookup update = dataFromTable;
			update.setCode(code);
			update.setName(name);
			update.setDescription(description);
	        dao.merge(update);
			context.getFlashScope().success("successfully.edited");
		}else//fail
			context.getFlashScope().error("failed");
		log.debug("=========================================success==============================================");
		return Results.redirect("/admin/lookupList");
    }
    	
    public Result lookupList() {
    	//List<LookupGroup> lookupGroups = dao.findAll(LookupGroup.class);
		return Results.html().render("listLookup", dao.findAll(Lookup.class, EnumOrder.ASC, "code"));
    }

    public Result getLookup(@Param("grpCode") String grpCode) {
		//List<ZebRoute> checkAvailble = dao.findByProperty(ZebRoute.class, "code", zebRouteDto.code);

    	List<Lookup> entityList = dao.findByProperty(Lookup.class,"lookupGroupCode", grpCode);
		return Results.json().render("lookup", entityList);
    }
    
}


