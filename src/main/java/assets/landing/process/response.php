<?php

if ($_SERVER['REQUEST_METHOD'] != 'POST') {
	header('Location: /');
	exit;
}

$servername = "localhost";
$username = "u867721831_zebra";
$password = "zebr4v4n99";

if(empty($_POST['name'])  		||
   empty($_POST['email']) 		||
   empty($_POST['phone']) 		||
   empty($_POST['pickroute'])	||
   !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
   {
	echo "No arguments Provided!";
	return false;
   }
	
$name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
$phone = $_POST['phone'];
$pickroute = $_POST['pickroute'];
$suggeststart = filter_var($_POST['suggeststart'], FILTER_SANITIZE_SPECIAL_CHARS);
$suggestdrop = filter_var($_POST['suggestdrop'], FILTER_SANITIZE_SPECIAL_CHARS);


try {
    $conn = new PDO("mysql:host=$servername;dbname=u867721831_zebra", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // prepare sql and bind parameters
    $stmt = $conn->prepare("INSERT INTO landingpage (name, email, phone, pickroute, suggeststart, suggestdrop) 
    VALUES (:name, :email, :phone, :pickroute, :suggeststart, :suggestdrop)");
    $stmt->bindParam(':name', $name);
    $stmt->bindParam(':email', $email);
    $stmt->bindParam(':phone', $phone);
    $stmt->bindParam(':pickroute', $pickroute);
    $stmt->bindParam(':suggeststart', $suggeststart);
    $stmt->bindParam(':suggestdrop', $suggestdrop);
// insert a row
    $stmt->execute();

    echo "Data has been saved successfully."; 
    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }
?>