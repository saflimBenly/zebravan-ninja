$(function() {
	$('#contactForm').validate({
	    rules: {
			name: {
				required: true,
				maxlength: 100,
				minlength: 4
			},
			email: {
				required: true,
				email: true,
				maxlength: 100
			},
			phone: { //find out about validation phone number
				required: true,
				maxlength: 50,
				minlength: 7
			},
			suggeststart: {
				required: true,
				maxlength: 50,
				minlength: 5
			},		  
			suggeststop: {
				required: true,
				maxlength: 50,
				minlength: 5
			},		  
		  
	    },
	    submitHandler: function(form) {
	        $("#btnSubmit").attr("disabled", true);
	        event.preventDefault();

	    	$('#contactForm').ajaxSubmit({
	            url: "/contact",
	            type: "POST",
	            data: $('#contactForm').serialize(),
	            cache: false,
	            success: function(data) {
	                // Enable button & show success message
	                $("#btnSubmit").attr("disabled", false);
	                $.bootstrapGrowl(data, {
	                    ele: 'body', // which element to append to
	                    type: 'success', // (null, 'info', 'danger', 'success')
	                    offset: {from: 'top', amount: 50}, // 'top', or 'bottom'
	                    align: 'right', // ('left', 'right', or 'center')
	                    width: 'auto', // (integer, or 'auto')
	                    delay: 3000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
	                    allow_dismiss: true, // If true then will display a cross to close the popup.
	                    stackup_spacing: 10 // spacing between consecutively stacked growls.
	                });

	                //clear all fields
	                $('.okvalid').hide();
	                $('#contactForm').trigger("reset");
	            },
	            error: function() {
	                // Fail message
	                $.bootstrapGrowl("Sorry, please try again later. Thanks", {
	                    ele: 'body', // which element to append to
	                    type: 'danger', // (null, 'info', 'danger', 'success')
	                    offset: {from: 'top', amount: 50}, // 'top', or 'bottom'
	                    align: 'right', // ('left', 'right', or 'center')
	                    width: 'auto', // (integer, or 'auto')
	                    delay: 3000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
	                    allow_dismiss: true, // If true then will display a cross to close the popup.
	                    stackup_spacing: 10 // spacing between consecutively stacked growls.
	                });
	                //clear all fields
	                $('#contactForm').trigger("reset");
	            },
	        });
	    },
	    highlight: function(element) {
			$(element).siblings('.okvalid').eq(0).hide();
			$(element).closest('.control-group').addClass('error');
		},
		success: function(element) {
			$(element).siblings('.okvalid').eq(0).show();
			$(element).closest('.control-group').removeClass('error');
		}
	  });
});
