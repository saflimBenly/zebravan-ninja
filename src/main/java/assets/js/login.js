/**
 * 
 */
window.fbAsyncInit = function() {
	FB.init({
		appId : '555403391329006',
		xfbml : true,
		version : 'v2.7'
	});
};

(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {
		return;
	}
	js = d.createElement(s);
	js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

$('#entry-facebook').click(function() {
	$.fblogin({
		fbId : '555403391329006',
		permissions : 'email,user_birthday,user_about_me',
		fields: 'id,first_name,last_name,locale,email,age_range,link,gender,timezone,updated_time,verified'
	}).fail(function(error) {
		console.log('error callback', error);
	}).progress(function(data) {
		console.log('progress', data);
	}).done(function(data) {
		//console.log('done everything', data);
		//alert(JSON.stringify(data, null, '\t'));
		$.post("/loginSNS", data)
		.done(function(result) {
			if (result.result == "not.register.sns"){
				alert("You are not register with FACEBOOK YET!");
			} else {
				//alert(result.result);
				window.location="/";
			}
		});
	});
});

var auth0 = new Auth0({
    domain:         'zebravan.au.auth0.com',
    clientID:       'T7AvK4whx7yYrAPucjsx4xBmSP1PQrUT',
    callbackURL:    'http://zebravan.co/callback'
});

$('#entry-google').click(function() {
	popupCenter('https://zebravan.au.auth0.com/authorize?response_type=token&client_id=T7AvK4whx7yYrAPucjsx4xBmSP1PQrUT&connection=google-oauth2&redirect_uri=https://zebravan.co/callback&state=login','Login Google+','600','460'); 
});

$('#entry-twitter').click(function() {
	popupCenter('https://zebravan.au.auth0.com/authorize?response_type=token&client_id=T7AvK4whx7yYrAPucjsx4xBmSP1PQrUT&connection=twitter&redirect_uri=https://zebravan.co/callback&state=login','Login Twitter','600','460'); 
});

$('#login').click(function() {
	$('#formLogin').submit();
	});
	
$('#ini').click(function() {
	$('#resetForm').submit();
	});
