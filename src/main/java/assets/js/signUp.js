/**
 * 
 */
window.fbAsyncInit = function() {
	FB.init({
		appId : '555403391329006',
		xfbml : true,
		version : 'v2.7'
	});
};

(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {
		return;
	}
	js = d.createElement(s);
	js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


$('#entry-facebook').click(function() {
	$.fblogin({
		fbId : '555403391329006',
		permissions : 'email,user_birthday',
		fields: 'id,first_name,last_name,locale,email,birthday',
	}).fail(function(error) {
		console.log('error callback', error);
	}).progress(function(data) {
		console.log('progress', data);
		console.log('progress2', data.status);
		if(data.status=="authenticate.fblogin"){
			console.log("success"+data.authResponse);
		}
	}).done(function(data) {
		console.log('done everything', data);
		$.post("/sign-up-fb", data)
		.done(function(result) {
			console.log(result.result + "dsdsa" )
			if (result.result == "not.register.fb"){
			} else if(result.result == "already"){ 
				window.location="/sign-up";
			} else {
				window.location="/";
			}
		});
	});
});

$('#entry-google').click(function() {
	popupCenter('https://zebravan.au.auth0.com/authorize?response_type=token&client_id=T7AvK4whx7yYrAPucjsx4xBmSP1PQrUT&connection=google-oauth2&redirect_uri=https://zebravan.co/callback&state=signup','Login Google+','600','460'); 
});

$('#entry-twitter').click(function() {
	popupCenter('https://zebravan.au.auth0.com/authorize?response_type=token&client_id=T7AvK4whx7yYrAPucjsx4xBmSP1PQrUT&connection=twitter&redirect_uri=https://zebravan.co/callback&state=signup','Login Twitter','600','460'); 
});

$(document).ready(function(){


	$('#loginForm').validate({
	errorClass: 'signuperror',
    rules: {
		fullname: {
			required: true
		},
		username: {
			required: true,
			email: true,
			minlength: 8
		},
		password: {
			required: true,
			minlength: 6
		},
		repassword: {
			required: true,
			minlength: 6,
			equalTo: "#password"
		},		  
		agree: "required"
	  
    },
	highlight: function(element) {
		$(element).closest('.control-group').removeClass('signupsuccess').addClass('signuperror');
	},
	success: function(element) {
		element
		.closest('.control-group').removeClass('signuperror').addClass('signupsuccess');
	}
  });

}); // end document.ready