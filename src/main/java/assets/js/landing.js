/**
 * 
 */
$(document).on('click', '#navigation li', function() {
    $("#navigation li").removeClass("active");
    $(this).addClass("active");
});

var s = skrollr.init({
    render: function(data) {
        //Log the current scroll position.
        console.log(data.curTop);
    	if(($("li#nav_update").hasClass("active") || $("li#nav_footer").hasClass("active")) && data.curTop > 1620) {
    		
    	} else {
	        if (data.curTop < 400) {
	        	$("#navigation li").removeClass("active");
	        	$("li#nav_home").addClass("active");
	        } else if (data.curTop < 900) {
	        	$("#navigation li").removeClass("active");
	        	$("li#nav_howitworks").addClass("active");
	        } else if (data.curTop < 1300) {
	        	$("#navigation li").removeClass("active");
	        	$("li#nav_features").addClass("active");
	        } else if (data.curTop < 1600) {
	        		$("#navigation li").removeClass("active");
	            	$("li#nav_whoweare").addClass("active");
	        } else if (data.curTop < 1784) {
	    		$("#navigation li").removeClass("active");
	        	$("li#nav_update").addClass("active");
	        } else if (data.curTop < 1912) {
	    		$("#navigation li").removeClass("active");
	        	$("li#nav_footer").addClass("active");
	        }
    	}
    }
});


skrollr.menu.init(s, {
	change: function(hash, top) {
		console.log(hash, top);
		if (hash == "#update") {
    		$("#navigation li").removeClass("active");
        	$("li#nav_update").addClass("active");
		}
		if (hash == "#footer") {
    		$("#navigation li").removeClass("active");
        	$("li#nav_footer").addClass("active");
		}
	}
});

