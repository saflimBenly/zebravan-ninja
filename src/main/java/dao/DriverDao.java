package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;

import dto.ZebvanDto;
import models.BaseModel;
import models.DailySchedule;
import models.Zebvan;
import ninja.jpa.UnitOfWork;

public class DriverDao extends GenericDao{

	public DriverDao() {
		super();
	}

	@UnitOfWork
	public <T extends BaseModel<?>> List<Zebvan> findVehicleByDriverId(Long driverId) {

		//Date today = new Date();
		String strQuery = "";
		strQuery = "select NEW models.Zebvan (zv.id, zv.name, zv.plateNo, zv.make, zv.model)"
					+ "from Zebvan zv join zv.driver where zv.driver.id = :driverId";
		
		List<Zebvan> query = getEntityManager().createQuery(strQuery, Zebvan.class).setParameter("driverId", driverId).getResultList();

		return query;
	}

	
	@UnitOfWork
	public <T extends BaseModel<?>> List<Zebvan> findBookingBlank(Long driverId) {

		//Date today = new Date();
		String strQuery = "";
		strQuery = "select NEW models.BookTrip (zv.id, zv.name, zv.plateNo, zv.make, zv.model)"
					+ "from Zebvan zv join zv.driver where zv.driver.id = :driverId";
		
		List<Zebvan> query = getEntityManager().createQuery(strQuery, Zebvan.class).setParameter("driverId", driverId).getResultList();

		return query;
	}
	
	@UnitOfWork
	public <T extends BaseModel<?>> ArrayList<ZebvanDto> findZebvanBasedOnTripSelected(Long tripId) {

		String strQuery = "";
		strQuery = "Select d.id as driverId, d.name as driverName, z.id, z.plateNo, z.color, "
				+ "(select name from lookup l where l.code = z.make) as make , "
				+ "(select name from lookup l where l.code = z.model) as model "
				+ "from DRIVERSCHEDULE "
				+ "ds inner join DRIVER d on d.id = ds.driver_id "
				+ "inner join ZEBVAN z on z.id = d.defaultvan_id "
				+ "where ds.trip_id = " + tripId;
		
//		List<Zebvan> query = getEntityManager().unwrap(Session.class).
				
		Session sess = getEntityManager().unwrap(Session.class);
		ArrayList<ZebvanDto> results = (ArrayList<ZebvanDto>) sess.createSQLQuery(strQuery)
										.addScalar("driverId", new LongType())
										.addScalar("driverName", new StringType())
										.addScalar("id", new LongType())
										.addScalar("plateNo", new StringType())
										.addScalar("make", new StringType())
										.addScalar("model", new StringType())
										.addScalar("color", new StringType())
										.setResultTransformer(Transformers.aliasToBean(ZebvanDto.class))
										.list();
		return results;
	}
	
	@UnitOfWork
	public <T extends BaseModel<?>> T findScheduleWithRoute(Class<T> clazz, Long id) {
	
		String strQuery = "";
		strQuery = "select NEW models.DailySchedule (ds.id, ds.tripdate, ds.triptime, ds.zebRoute.id, ds.zebRoute.code, ds.zebRoute.name, "
				+ "CASE WHEN (ds.triptime >= '06:00' AND ds.triptime <= '10:00') THEN 'Morning' ELSE 'Evening' END as scheduleType) "
				+ "from DailySchedule ds join ds.zebRoute where ds.id = :id";

		return getEntityManager().createQuery(strQuery, clazz).setParameter("id", id).getSingleResult();
	}
	
	
}
