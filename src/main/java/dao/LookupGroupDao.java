/**
 * Copyright (C) 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dao;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;

import dto.LookupGroupDto;
import models.Lookup;
import models.LookupGroup;
import ninja.jpa.UnitOfWork;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

public class LookupGroupDao extends GenericDao{
	private Log log = LogFactory.getLog(CategoryDao.class);
   
    public List<LookupGroup> getAllLookupGroup() {
        List<LookupGroup> lookupGroups = findAll(LookupGroup.class);
        return lookupGroups;
    }
    
    public LookupGroup getLookupGroupById(Long id) {
        LookupGroup lookupGroup = find(LookupGroup.class, id);
        return lookupGroup;
    }

    public LookupGroup getLookupGroupByCode(String code) {
        LookupGroup lookupGroup = findOneByProperty(LookupGroup.class, "code", code);
        return lookupGroup;
    }

    /**
     * Returns false if failed
     */
    public boolean saveLookupGroup(LookupGroupDto lookupGroupDto) {
        LookupGroup lookupGroup = new LookupGroup(lookupGroupDto.code, lookupGroupDto.description);
        save(lookupGroup);
        return true;
        
    }

    public void deleteLookupGroup(LookupGroup lookupGroup ) {
    	 delete(LookupGroup.class, lookupGroup.getId());
    }
}
