/**
 * Copyright (C) 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dao;

import java.util.List;

import dto.LookupDto;
import models.Lookup;
import models.LookupGroup;

public class LookupDao extends GenericDao {
   
    
    public List<Lookup> getAllLookup() {
        List<Lookup> lookups = findAll(Lookup.class);
        return lookups;
    }
    
    public Lookup getLookupById(Long id) {

        Lookup lookup = find(Lookup.class, id);

        return lookup;
    }

    public Lookup getLookupByCode(String code) {

        Lookup lookup = findOneByProperty(Lookup.class, "code", code);

        return lookup;
    }

    public boolean saveLookup(LookupDto lookupDto) {

        LookupGroup lookupGroup = findOneByProperty(LookupGroup.class, "code", lookupDto.lookupGroupCode);
        Lookup lookup = new Lookup();
        save(lookup);
        
        return true;
        
    }

}
