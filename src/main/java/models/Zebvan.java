
package models;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Zebvan extends BaseModel<Long> {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	private String code;
	private String name;
	private String model;
	private String kirNo;
	private Date kirExp;
	private int year;
	private String description;
	private String color;			
	private String make;	
	private String plateNo;			
	private String stnkNo;			
	private Date stnkExp;			
	private int seatAvailable;	
	private String imagePath;

	@ManyToOne
    @JoinColumn(name="driver_id")
    private Driver driver;
	
	
	@ManyToOne
    @JoinColumn(name="partner_id")
    private Partner partner;

    public Zebvan() {
	}
    
    public Zebvan(long id, String name, String plateNo, String make, String model, long driverId, String driverName) {
    	this.id = id;
    	this.name = name;
    	this.plateNo = plateNo;
    	this.make = make;
    	this.model = model;
    	
    	this.driver = new Driver();
    	this.driver.setId(driverId);
    	this.driver.setName(driverName);
    }

    public Zebvan(long id, String name, String plateNo, String make, String model) {
    	this.id = id;
    	this.name = name;
    	this.plateNo = plateNo;
    	this.make = make;
    	this.model = model;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getKirNo() {
		return kirNo;
	}

	public void setKirNo(String kirNo) {
		this.kirNo = kirNo;
	}

	public Date getKirExp() {
		return kirExp;
	}

	public void setKirExp(Date kirExp) {
		this.kirExp = kirExp;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getPlateNo() {
		return plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public String getStnkNo() {
		return stnkNo;
	}

	public void setStnkNo(String stnkNo) {
		this.stnkNo = stnkNo;
	}

	public Date getStnkExp() {
		return stnkExp;
	}

	public void setStnkExp(Date stnkExp) {
		this.stnkExp = stnkExp;
	}

	public int getSeatAvailable() {
		return seatAvailable;
	}

	public void setSeatAvailable(int seatAvailable) {
		this.seatAvailable = seatAvailable;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	@JsonIgnore
	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	@JsonIgnore
	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}	

}
