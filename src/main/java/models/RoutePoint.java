package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by saflim on 06/05/2016.
 */
@Entity
public class RoutePoint extends BaseModel<Long> {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private int locOrder;
    private String locName="";
    private String locPoint="";
    private String locDisplay="";
    
    @ManyToOne
    @JoinColumn(name="route_id")
    private ZebRoute zebRoute;

    public RoutePoint() {
		super();
    }
    
    public RoutePoint(Long id) {
		super();
		this.id = id;
	}
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getLocOrder() {
		return locOrder;
	}

	public void setLocOrder(int locOrder) {
		this.locOrder = locOrder;
	}

	public String getLocName() {
		return locName;
	}

	public void setLocName(String locName) {
		this.locName = locName;
	}

	public String getLocPoint() {
		return locPoint;
	}

	public void setLocPoint(String locPoint) {
		this.locPoint = locPoint;
	}
	
	public String getLocDisplay() {
		return locDisplay;
	}

	public void setLocDisplay(String locDisplay) {
		this.locDisplay = locDisplay;
	}

	@JsonIgnore
    public ZebRoute getZebRoute() {
        return zebRoute;
    }

    public void setZebRoute(ZebRoute zebRoute) {
        this.zebRoute = zebRoute;
    }
}
