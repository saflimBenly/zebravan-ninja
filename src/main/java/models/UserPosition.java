
package models;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class UserPosition extends BaseModel<Long> {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String locPoint="";
    private Timestamp timerecord;
    
	@ManyToOne
    @JoinColumn(name="member_id")
    private Member member;

    public UserPosition() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLocPoint() {
		return locPoint;
	}

	public void setLocPoint(String locPoint) {
		this.locPoint = locPoint;
	}

	public Timestamp getTimerecord() {
		return timerecord;
	}

	public void setTimerecord(Timestamp timerecord) {
		this.timerecord = timerecord;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}	
}
