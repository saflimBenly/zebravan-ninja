package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
public class Landing extends BaseModel<Long> {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	private String name;
	private String email;
	private String phone;
	private String pickroute;
	private String suggeststart;
	private String suggestdrop;
	
	public Landing() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPickroute() {
		return pickroute;
	}

	public void setPickroute(String pickroute) {
		this.pickroute = pickroute;
	}

	public String getSuggeststart() {
		return suggeststart;
	}

	public void setSuggeststart(String suggeststart) {
		this.suggeststart = suggeststart;
	}

	public String getSuggestdrop() {
		return suggestdrop;
	}

	public void setSuggestdrop(String suggestdrop) {
		this.suggestdrop = suggestdrop;
	}
	
}
