
package models;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "ROUTE")
public class ZebRoute extends BaseModel<Long> {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	private String code;
	private String name;
	private String fromname;
	private String frompoint;
	private String toname;
	private String topoint;
	private int price;
	private String description;
    private String imagepath;
    
    private String fromdisplay; 
    private String todisplay;
    
    @OneToMany(cascade = CascadeType.ALL,
        	mappedBy = "zebRoute", 
        	orphanRemoval = true, 
        	targetEntity = RoutePoint.class, 
        	fetch = FetchType.EAGER)
    private List<RoutePoint> routePoint;

    @OneToMany(cascade = CascadeType.ALL,
        	mappedBy = "zebRoute", 
        	orphanRemoval = true, 
        	targetEntity = DailySchedule.class, 
        	fetch = FetchType.LAZY)
    private List<DailySchedule> dailySchedule;

    public ZebRoute() {
	}

	public ZebRoute(Long id){
		this.setId(id);
		this.setCode("");
		this.setName("");
		this.setFromname("");
		this.setFrompoint("");
		this.setToname("");
		this.setTopoint("");
		this.setDescription("");
		this.setRoutePoint(new ArrayList<RoutePoint>());
		for (int i=0;i<6;i++) {
			this.getRoutePoint().add(new RoutePoint(new Long(i)));
		}
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFromname() {
		return fromname;
	}

	public void setFromname(String fromname) {
		this.fromname = fromname;
	}

	public String getFrompoint() {
		return frompoint;
	}

	public void setFrompoint(String frompoint) {
		this.frompoint = frompoint;
	}

	public String getToname() {
		return toname;
	}

	public void setToname(String toname) {
		this.toname = toname;
	}

	public String getTopoint() {
		return topoint;
	}

	public void setTopoint(String topoint) {
		this.topoint = topoint;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<RoutePoint> getRoutePoint() {
		return routePoint;
	}

	public void setRoutePoint(List<RoutePoint> routePoint) {
		this.routePoint = routePoint;
	}

	public List<DailySchedule> getDailySchedule() {
		return dailySchedule;
	}

	public void setDailySchedule(List<DailySchedule> dailySchedule) {
		this.dailySchedule = dailySchedule;
	}

	public String getImagepath() {
		return imagepath;
	}

	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}

	public String getFromdisplay() {
		return fromdisplay;
	}

	public void setFromdisplay(String fromdisplay) {
		this.fromdisplay = fromdisplay;
	}

	public String getTodisplay() {
		return todisplay;
	}

	public void setTodisplay(String todisplay) {
		this.todisplay = todisplay;
	}
}
