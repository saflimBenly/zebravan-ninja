package models;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by safaridinsal on 31/07/2015.
 */
@Entity
public class MainBillboard extends BaseModel<Long> {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private Date startdate;
    private Date enddate;
    private String name;
    private String description;
    private String url;
    @OneToMany
    private Set<Carousel> carousel;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Set<Carousel> getCarousel() {
        return carousel;
    }

    public void setCarousel(Set<Carousel> carousel) {
        this.carousel = carousel;
    }
}
