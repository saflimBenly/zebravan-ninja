package models;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by safaridinsal on 28/07/2015.
 */
@Entity
public class Lookup extends BaseModel<Long> {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private String code;
    private String name;
    private String description;
    
    @Column(name="lookupgroup_code")
    private String lookupGroupCode;
    
    
    @ManyToOne(optional = false)
    private LookupGroup lookupGroup;

    public Lookup() {
    }

    public Lookup(Long lookupgroup_id, String code, String name, String description) {
    	this.code = code;
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String lookupCode) {
        this.code = lookupCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String lookupName) {
        this.name = lookupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonIgnore
    public LookupGroup getLookupGroup() {
        return lookupGroup;
    }

    public void setLookupGroup(LookupGroup lookupGroup) {
        this.lookupGroup = lookupGroup;
    }

	public String getLookupGroupCode() {
		return lookupGroupCode;
	}

	public void setLookupGroupCode(String lookupGroupCode) {
		this.lookupGroupCode = lookupGroupCode;
	}
    
}
