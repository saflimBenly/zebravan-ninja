package models;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by safaridinsal on 31/07/2015.
 */
@Entity
public class ServiceProvider extends BaseModel<Long> {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private String name;
    private String storename;
    private String description;
    @OneToOne
    private Member member;
    @OneToMany
    private Set<ServiceProduct> serviceproduct;
    private String logo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Set<ServiceProduct> getServiceproduct() {
        return serviceproduct;
    }

    public void setServiceproduct(Set<ServiceProduct> serviceproduct) {
        this.serviceproduct = serviceproduct;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
