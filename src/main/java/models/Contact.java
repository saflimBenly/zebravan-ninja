package models;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by safaridinsal on 31/07/2015.
 */
@Entity
public class Contact extends BaseModel<Long> {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private String contacttype;
    private String detail;
    private String channel;
    @ManyToOne
    private Member member;
    
    public Contact() {
		super();
    }
    
    public Contact(Long id) {
		super();
		this.id = id;
		this.contacttype = "";
		this.detail = "";
		this.channel = "";
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContacttype() {
        return contacttype;
    }

    public void setContacttype(String contacttype) {
        this.contacttype = contacttype;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    @JsonIgnore
    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }
}
