
package models;
import java.sql.Date;
import java.sql.Time;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class DailyScheduleView extends BaseModel<Long> {

	private Long id;
	private Long routeid;
	private String routename;
	private Date tripdate;
	private Time triptime;
	private Integer allocation;
	private Integer booked;
	private Integer allocated;
	private Integer handled;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getRouteid() {
		return routeid;
	}
	public void setRouteid(Long routeid) {
		this.routeid = routeid;
	}
	public String getRoutename() {
		return routename;
	}
	public void setRoutename(String routename) {
		this.routename = routename;
	}
	public Date getTripdate() {
		return tripdate;
	}
	public void setTripdate(Date tripdate) {
		this.tripdate = tripdate;
	}
	public Time getTriptime() {
		return triptime;
	}
	public void setTriptime(Time triptime) {
		this.triptime = triptime;
	}
	public Integer getBooked() {
		return booked;
	}
	public void setBooked(Integer booked) {
		this.booked = booked;
	}
	public Integer getAllocated() {
		return allocated;
	}
	public void setAllocated(Integer allocated) {
		this.allocated = allocated;
	}
	public Integer getHandled() {
		return handled;
	}
	public void setHandled(Integer handled) {
		this.handled = handled;
	}
	public Integer getAllocation() {
		return allocation;
	}
	public void setAllocation(Integer allocation) {
		this.allocation = allocation;
	}
	
}
