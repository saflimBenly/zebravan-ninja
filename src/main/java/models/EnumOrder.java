package models;

public enum EnumOrder {
	ASC, DESC;

	public boolean isAscOrder() {
		return ASC.equals(this);
	}
}
