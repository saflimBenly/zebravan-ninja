package models;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by saflim on 06/05/2016.
 */
@Entity
public class Driver extends BaseModel<Long> {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    private String name;
    private String ktpNo;
    private String simNo;
    private String ktpImg;
    private String simImg;
    private String phoneNo;
    private String deviceKey;

    @OneToOne
    @JoinColumn(name="member_id")
    private Member member;

    @ManyToOne
    @JoinColumn(name="partner_id")
    private Partner partner;
    
    @OneToOne
    @JoinColumn(name="defaultvan_id")
	private Zebvan defaultvan;
    
    @ElementCollection(fetch=FetchType.EAGER)
    @CollectionTable(name="DriverSchedule", joinColumns=@JoinColumn(name="driver_id"))
    @Column(name="trip_id")
    private List<Long> scheduleIds;
    
//    @OneToMany(cascade = CascadeType.ALL,
//        	mappedBy = "driver", 
//        	orphanRemoval = true, 
//        	targetEntity = Zebvan.class, 
//        	fetch = FetchType.EAGER)
//    private Set<Zebvan> zebvan;

    public Driver() {
		super();
    }
    
    public Driver(Long id) {
		super();
		this.id = id;
	}
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKtpNo() {
		return ktpNo;
	}

	public void setKtpNo(String ktpNo) {
		this.ktpNo = ktpNo;
	}

	public String getSimNo() {
		return simNo;
	}

	public void setSimNo(String simNo) {
		this.simNo = simNo;
	}

	public String getKtpImg() {
		return ktpImg;
	}

	public void setKtpImg(String ktpImg) {
		this.ktpImg = ktpImg;
	}

	public String getSimImg() {
		return simImg;
	}

	public void setSimImg(String simImg) {
		this.simImg = simImg;
	}

	@JsonIgnore
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	@JsonIgnore
	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}
	
	public Zebvan getDefaultvan() {
		return defaultvan;
	}

	public void setDefaultvan(Zebvan defaultvan) {
		this.defaultvan = defaultvan;
	}

	public List<Long> getScheduleIds() {
		return scheduleIds;
	}

	public void setScheduleIds(List<Long> scheduleIds) {
		this.scheduleIds = scheduleIds;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getDeviceKey() {
		return deviceKey;
	}

	public void setDeviceKey(String deviceKey) {
		this.deviceKey = deviceKey;
	}
}
