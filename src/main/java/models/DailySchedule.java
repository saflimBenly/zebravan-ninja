
package models;
import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "DAILYTRIP")
public class DailySchedule extends BaseModel<Long> {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	private Date tripdate;
	private Time triptime;
	private Integer allocation;
	@Transient
	private Boolean booked;
	@Transient
	private String scheduleType;
	@Transient
	private long bookedCnt;
	@Transient
	private long handledCnt;
	@Transient
	private long allocatedCnt;
	@Transient
	private String pickup;
	
    @ManyToOne
    @JoinColumn(name="route_id")
    private ZebRoute zebRoute;

    @ManyToOne
    private Zebvan zebvan;

    @ManyToOne
    private Driver driver;
    
    public DailySchedule() {
	}

    public DailySchedule(long id, java.util.Date tripdate, java.util.Date triptime, long zebRouteId, String zebRouteCode, String zebRouteName, String scheduleType) {

		this.id = id;
    	this.tripdate = new java.sql.Date(tripdate.getTime());
    	this.triptime = new java.sql.Time(triptime.getTime());
    	this.zebRoute = new ZebRoute();
    	this.zebRoute.setId(zebRouteId);
    	this.zebRoute.setCode(zebRouteCode);
    	this.zebRoute.setName(zebRouteName);
    	this.scheduleType = scheduleType;
    }

    public DailySchedule(long id, java.util.Date tripdate, java.util.Date triptime, long zebRouteId, String zebRouteCode, String zebRouteName, 
    		Integer allocation, long bookedCnt, long handledCnt, long allocatedCnt) {

		this.id = id;
    	this.tripdate = new java.sql.Date(tripdate.getTime());
    	this.triptime = new java.sql.Time(triptime.getTime());
    	if (allocation != null) this.allocation = allocation;
    	this.bookedCnt = bookedCnt;
    	this.handledCnt = handledCnt;
    	this.allocatedCnt = allocatedCnt;
    	
    	this.zebRoute = new ZebRoute();
    	this.zebRoute.setId(zebRouteId);
    	this.zebRoute.setCode(zebRouteCode);
    	this.zebRoute.setName(zebRouteName);

    	
    }
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Date getTripdate() {
		return tripdate;
	}


	public void setTripdate(Date tripdate) {
		this.tripdate = tripdate;
	}


	public Time getTriptime() {
		return triptime;
	}


	public void setTriptime(Time triptime) {
		this.triptime = triptime;
	}


	@JsonIgnore
    public ZebRoute getZebRoute() {
        return zebRoute;
    }

    public void setZebRoute(ZebRoute zebRoute) {
        this.zebRoute = zebRoute;
    }

    @JsonIgnore
	public Zebvan getZebvan() {
		return zebvan;
	}

	public void setZebvan(Zebvan zebvan) {
		this.zebvan = zebvan;
	}

	@JsonIgnore
	public Driver getDriver() {
		return driver;
	}

	public Boolean getBooked() {
		return booked;
	}


	public void setBooked(Boolean booked) {
		this.booked = booked;
	}


	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public Integer getAllocation() {
		return allocation;
	}

	public void setAllocation(Integer allocation) {
		this.allocation = allocation;
	}


	public String getScheduleType() {
		return scheduleType;
	}


	public void setScheduleType(String scheduleType) {
		this.scheduleType = scheduleType;
	}

	public long getBookedCnt() {
		return bookedCnt;
	}

	public void setBookedCnt(long bookedCnt) {
		this.bookedCnt = bookedCnt;
	}

	public long getHandledCnt() {
		return handledCnt;
	}

	public void setHandledCnt(long handledCnt) {
		this.handledCnt = handledCnt;
	}

	public long getAllocatedCnt() {
		return allocatedCnt;
	}

	public void setAllocatedCnt(long allocatedCnt) {
		this.allocatedCnt = allocatedCnt;
	}

	public String getPickup() {
		return pickup;
	}

	public void setPickup(String pickup) {
		this.pickup = pickup;
	}

}
