-- the first script for migration
create table Member (
    id bigserial primary key,
    fullname varchar(255),
    isAdmin boolean NOT NULL,
    password varchar(255),
    username varchar(255),
    salt character varying(50), 
	fbid character varying(35), 
	firstname varchar(100), 
	lastname varchar(100), 
	birthdate date, 
	gender character(2), 
	fblogin boolean,
	profile VARCHAR(150),
	status VARCHAR(2),
	certificate VARCHAR(255),
	imagepath VARCHAR(255),
	image BYTEA
);

-- Address
CREATE TABLE Address (
  id bigserial primary key,
  addresstype VARCHAR(20),
  addressline1 VARCHAR(200),
  addressline2 VARCHAR(200),
  district VARCHAR(20),
  city VARCHAR(20),
  province VARCHAR(20),
  country VARCHAR(20),
  postcode VARCHAR(10),
  member_id BIGSERIAL references Member(id)
);

-- Contact
CREATE TABLE Contact (
  id bigserial primary key,
  contacttype VARCHAR(20),
  detail VARCHAR(100),
  channel VARCHAR(20),
  member_id BIGSERIAL REFERENCES Member(id)
);

-- MemberSocmed
CREATE TABLE MemberSocmed (
  id bigserial primary key,
  socmedno VARCHAR(50),
  resfield1 VARCHAR(50),
  resfield2 VARCHAR(50),
  resfield3 VARCHAR(150),
  resfield4 VARCHAR(100),
  member_id BIGSERIAL REFERENCES Member(id)
);

-- PinMarker
CREATE TABLE PinMarker (
  id bigserial primary key,
  code VARCHAR(30),
  name VARCHAR(100),
  image VARCHAR(100),
  lat double precision,
  lng double precision,
  description VARCHAR(200)
);

-- Route
CREATE TABLE Route (
  id bigserial primary key,
  code VARCHAR(30),
  name VARCHAR(100),
  routepoint VARCHAR(300),
  pickuppoint VARCHAR(200),
  dropoffpoint VARCHAR(200)
);

-- Partner
CREATE TABLE Partner (
  id bigserial primary key,
  name VARCHAR(100),
  companyname VARCHAR(100),
  description VARCHAR(200),
  member_id BIGSERIAL REFERENCES Member(id)
);

-- Driver
CREATE TABLE Driver (
  id bigserial primary key,
  name VARCHAR(100),
  ktpno VARCHAR(30),
  simno VARCHAR(30),
  ktpimg VARCHAR(100),
  simimg varchar(100),
  member_id BIGSERIAL REFERENCES Member(id),
  partner_id BIGSERIAL REFERENCES Partner(id)
);

-- Zebvan
CREATE TABLE Zebvan (
  id bigserial primary key,
  code VARCHAR(50),
  name VARCHAR(100),
  brand VARCHAR(30),
  model VARCHAR(30),
  color VARCHAR(30),
  kirno VARCHAR(30),
  kirexp date,
  year smallint,
  description VARCHAR(100),
  partner_id BIGSERIAL REFERENCES Partner(id)
);

-- DailyTrip
CREATE TABLE DailyTrip (
  id bigserial primary key,
  starttime date,
  endtime date,
  route_id BIGSERIAL REFERENCES Route(id),
  zebvan_id BIGSERIAL REFERENCES Zebvan(id),
  driver_id BIGSERIAL REFERENCES Driver(id)
);

CREATE TABLE BookTrip (
  id bigserial primary key,
  bookdate date,
  cost INTEGER,
  discount INTEGER,
  promocode VARCHAR(100),
  paymenttype VARCHAR(20),
  status VARCHAR(10),
  member_id BIGSERIAL REFERENCES Member(id),
  trip_id BIGSERIAL REFERENCES DailyTrip(id)
);
CREATE TABLE BookHist (
  id bigserial primary key,
  prestatus VARCHAR(10),
  curstatus VARCHAR(10),
  transdate date,
  member_id BIGSERIAL REFERENCES Member(id),
  booktrip_id BIGSERIAL REFERENCES BookTrip(id)
);


-- Trip Review
CREATE TABLE TripReview (
	rating smallint,
	content VARCHAR(400),
	flag smallint,
  	member_id BIGSERIAL REFERENCES Member(id),
	trip_id BIGSERIAL REFERENCES DailyTrip(id)
);

create table LookupGroup (
    id bigserial primary key,
    code varchar(10) NOT NULL UNIQUE,
    description varchar(100)
);

create table Lookup (
    id bigserial primary key,
	lookupgroup_code varchar(10) references LookupGroup(code), 
	description character varying(100),
    lookupgroup_id BIGSERIAL references LookupGroup(id),
    code varchar(10) NOT NULL,
    name varchar(100)
);

create table NewsLetter (
    id bigserial primary key,
    email varchar(255),
    createdt date
);

create table Landing 
(
  id bigserial primary key,
  name varchar(100),
  email varchar(50),
  phone varchar(30),
  pickroute varchar(10),
  suggeststart varchar(50),
  suggestdrop varchar(50)
);
  
-- to be deleted, not related with apps
create table Article (
    id bigserial primary key,
    content varchar(5000) NOT NULL,
    postedAt timestamp,
    title varchar(255) NOT NULL
);

-- to be deleted, not related with apps
create table Article_authorIds (
    Article_id bigserial references Article (id),
    authorIds bigserial references Member (id)
);