----- ADD 6-May-2016

-- alter table Route
ALTER TABLE Route 
  DROP COLUMN routepoint,
  DROP COLUMN pickuppoint,
  DROP COLUMN dropoffpoint,
  ADD COLUMN fromname VARCHAR(200),
  ADD COLUMN frompoint VARCHAR(200),
  ADD COLUMN toname VARCHAR(200),
  ADD COLUMN topoint VARCHAR(200),
  ADD COLUMN price integer,
  ADD COLUMN description VARCHAR(300)
;

-- alter table DailyTrip
ALTER TABLE DailyTrip 
  DROP COLUMN endtime,
  DROP COLUMN starttime,
  ADD COLUMN tripdate date, 
  ADD COLUMN triptime time
;

-- create table RoutePoint
CREATE TABLE RoutePoint 
(
  id bigserial primary key,
  locorder integer,
  locname VARCHAR(300),
  locpoint VARCHAR(100),
  route_id BIGSERIAL REFERENCES Route(id)
);

----- ADD 18-May-2016
-- alter table DailyTrip
ALTER TABLE DailyTrip 
  ADD COLUMN allocation integer,
  ALTER COLUMN zebvan_id DROP DEFAULT,
  ALTER COLUMN zebvan_id DROP NOT NULL, 
  ALTER COLUMN driver_id DROP DEFAULT, 
  ALTER COLUMN driver_id DROP NOT NULL
;

ALTER TABLE Driver
  ALTER COLUMN partner_id DROP DEFAULT, 
  ALTER COLUMN partner_id DROP NOT NULL
;

ALTER TABLE Zebvan
  ALTER COLUMN partner_id DROP DEFAULT, 
  ALTER COLUMN partner_id DROP NOT NULL
;

-- 3-June-2016
create table DriverSchedule (
    driver_id bigserial references Driver(id),
    trip_id bigserial references DailyTrip(id)
);

-- 5-June-2016
ALTER TABLE Zebvan
  DROP COLUMN brand,
  ADD COLUMN make varchar(50), 
  ADD COLUMN plateno varchar(20), 
  ADD COLUMN stnkno varchar(50), 
  ADD COLUMN stnkexp date,
  ADD COLUMN seatavailable integer,
  ADD COLUMN imagepath VARCHAR(255),
  ADD COLUMN driver_id BIGSERIAL references Driver(id)
;

-- 10-June-2016
ALTER TABLE Lookup
    ALTER COLUMN code TYPE VARCHAR(50)
;

-- 29-June-2016
ALTER TABLE Zebvan
  ADD COLUMN defaultvan boolean
;

ALTER TABLE Driver
  ADD COLUMN zebvan_id BIGSERIAL references Zebvan(id)
;

ALTER TABLE Driver
  ALTER COLUMN zebvan_id DROP DEFAULT, 
  ALTER COLUMN zebvan_id DROP NOT NULL
;

ALTER TABLE Booktrip 
  ADD COLUMN zebvan_id bigint references Zebvan(id) NULL,
  ADD COLUMN driver_id bigint references Driver(id) NULL
;

ALTER TABLE BookTrip
  ALTER COLUMN zebvan_id DROP DEFAULT, 
  ALTER COLUMN zebvan_id DROP NOT NULL
;

ALTER TABLE BookTrip
  ALTER COLUMN driver_id DROP DEFAULT, 
  ALTER COLUMN driver_id DROP NOT NULL
;

-- 12-Aug-2016
ALTER TABLE Driver
	ADD COLUMN defaultvan_id bigint references Zebvan(id) NULL
;

ALTER TABLE Driver
  ALTER COLUMN defaultvan_id DROP DEFAULT, 
  ALTER COLUMN defaultvan_id DROP NOT NULL
;

-- 25-Aug-2016
-- alter table Route
ALTER TABLE Route 
  ADD COLUMN imagepath VARCHAR(255)
;

-- 6-Sep-2016
ALTER TABLE Driver 
  ADD COLUMN phoneNo VARCHAR(50),
  ADD COLUMN deviceKey VARCHAR(50)
;

-- 8-Sep-2016
CREATE TABLE UserPosition 
(
  id bigserial primary key,
  locpoint VARCHAR(100),
  timerecord TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),  
  member_id BIGSERIAL REFERENCES Member(id)
);

-- 13-Dec-2016
-- alter table RoutePoint
ALTER TABLE RoutePoint 
  ADD COLUMN locdisplay VARCHAR(100)
;

ALTER TABLE Route 
  ADD COLUMN fromdisplay VARCHAR(100),
  ADD COLUMN todisplay VARCHAR(100)
;

ALTER TABLE Booktrip 
  ADD COLUMN pickup VARCHAR(100)
;

-- 16-Jan-2017
ALTER TABLE Member 
	ADD COLUMN isDriver boolean NOT NULL DEFAULT 'FALSE'
;

