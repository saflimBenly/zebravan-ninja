/**
 * Copyright (C) 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dao;

import static org.junit.Assert.assertEquals;

import java.util.List;

import models.EnumOrder;
import models.LookupGroup;
import models.Member;
import ninja.NinjaTest;

import org.junit.Test;

import com.google.inject.Injector;

public class AbstractDAOTest extends NinjaTest {
	
    //@Test
    public void testAbstractDAO(){
        Injector applicationInjector = getInjector();

        // Access internal dao
        GenericDao dao = applicationInjector.getInstance(GenericDao.class);
        
        //save(class)
        Member member = new Member();
        member.setFirstname("John");
        member.setLastname("Doe");
        member.setUsername("johnDoe@gmail.com");
        //member.setAdmin(false);
        //System.out.println(">>> Save Id: " + dao.save(member));
        
        //merge(class)
        member.setFullname("Ajudan Gue");
        member.setGender("GN");
        member = dao.merge(member);
        
        System.out.println(">>> Merge Id: " + member.getId());
        
        //delete(class, id)
        //dao.delete(Member.class, member.getId());
                
        //find(class, id)
        member = dao.find(Member.class, new Long(1));
        System.out.println(">>> member FirstName: " + member.getUsername());
        assertEquals(member.getUsername(), "bob@gmail.com");
        
        //findByProperty(class, property, value)
        List<Member> listMember1 = dao.findByProperty(Member.class,"fullname","Bob");
        for (Member member1 : listMember1) {
            System.out.println(">>>> member1: "+ member1.getFullname());
        }

        //findByProperty(class, property, value, match)
        List<Member> listMember2 = dao.findByProperty(Member.class,"fullname","A", GenericDao.MatchMode.START);
        for (Member member2 : listMember2) {
            System.out.println(">>>> member2: "+ member2.getFullname());
        }

        //findAll(class)
		List<Member> listMember3 = dao.findAll(Member.class);
        for (Member member3 : listMember3) {
            System.out.println(">>>> member3: " + member3.getFullname());
        }

        //findAll(class, order, property...)'
        List<Member> listMember4 = dao.findAll(Member.class, EnumOrder.ASC, "fullname");
        for (Member member4 : listMember4) {
            System.out.println(">>>> member4: " + member4.getFullname());
        }

        //findAllPagination(class, order, from, to)

    }

}
