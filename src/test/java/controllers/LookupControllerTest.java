/**
 * Copyright (C) 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers;

import java.util.List;

import models.LookupGroup;
import ninja.NinjaTest;

import org.junit.Test;

import com.google.inject.Injector;

import dao.GenericDao;

public class LookupControllerTest extends NinjaTest {

    //@Test
    public void testLookupGroup(){
        // Get the application injector
        Injector applicationInjector = getInjector();

        // Access internal dao
        GenericDao dao = applicationInjector.getInstance(GenericDao.class);
        
        LookupGroup lg = new LookupGroup();
        lg.setCode("GN");
        lg.setDescription("Test");

        dao.save(lg);
        
        lg = null;
        lg = new LookupGroup();
        lg.setCode("AD");
        lg.setDescription("Test lagi");
        dao.save(lg);
        
        lg = null;
        lg = new LookupGroup();
        lg.setCode("BD");
        lg.setDescription("Ada test lagi");
        dao.save(lg);
        
        //dao.delete(LookupGroup.class, lg.getId());

        List<LookupGroup> listLg = dao.findAll(LookupGroup.class);
        
        System.out.println(">>> ListLg: " + listLg.size());
        
        //assertEquals(dao.getLookupGroupByCode("TST").getDescription(), "This is for test purpose");
        System.out.println(">>>> SUCCESS LOOKUP GROUP <<<<");

    }

    //@Test
    public void testLookup(){
//        // Get the application injector
//        Injector applicationInjector = getInjector();
//
//        // Access internal dao
//        LookupDao lookupDao = applicationInjector.getInstance(LookupDao.class);
//        LookupGroupDao lookupGroupDao = applicationInjector.getInstance(LookupGroupDao.class);
//        assertTrue(lookupGroupDao.saveLookupGroup(new LookupGroupDto("TST", "This is for test purpose")));
//
//        LookupDto lookupDto = new LookupDto();
//        lookupDto.lookupGroupCode = "TST";
//        lookupDto.code = "TST01";
//        lookupDto.name = "Test First";
//        lookupDto.description = "Test the first one";
//
//        assertTrue(lookupDao.saveLookup(lookupDto));
//        assertEquals(lookupDao.getLookupByCode("TST01").getName(), "Test First");
//
//        Lookup lookup = lookupDao.getLookupByCode("TST01");
//        System.out.println(">>>> get LookupGroup: " + lookup.getLookupGroup().getDescription() + " <<<<");
//
//        lookupDto.lookupGroupCode = "TST";
//        lookupDto.code = "TST02";
//        lookupDto.name = "Test Second";
//        lookupDto.description = "Test the second one";
//        lookupDao.saveLookup(lookupDto);
//
//        assertEquals(lookupDao.getAllLookup().size(), 2);
    }
}
